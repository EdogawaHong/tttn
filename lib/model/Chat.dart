import 'package:tttn/model/ChatItem.dart';

class Chat {
  int idMess;
  int idReceiver;
  int idSender;
  List<ChatItem> items;

  Chat({this.idMess, this.idReceiver, this.idSender, this.items});

  Chat.fromJson(Map<String, dynamic> json) {
    idMess = json['id_mess'];
    idReceiver = json['id_receiver'];
    idSender = json['id_sender'];
    if (json['items'] != null) {
      items = new List<ChatItem>();
      json['items'].forEach((v) {
        items.add(new ChatItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_mess'] = this.idMess;
    data['id_receiver'] = this.idReceiver;
    data['id_sender'] = this.idSender;
    if (this.items != null) {
      data['items'] = this.items.map((v) => v.toJson()).toList();
    }
    return data;
  }

}