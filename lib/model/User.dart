import 'Offer.dart';

class User {
  int idUser;
  String name;
  String phone;
  String email;
  String password;
  String avatar;
  Offer offer;

  User(
      {this.idUser,
        this.name,
        this.phone,
        this.email,
        this.password,
        this.avatar,
        this.offer});

  User.fromJson(Map<String, dynamic> json) {
    idUser = json['id_user'];
    name = json['name'];
    phone = json['phone'];
    email = json['email'];
    password = json['password'];
    avatar = json['avatar'];
    offer = json['offer'] != null ? new Offer.fromJson(json['offer']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_user'] = this.idUser;
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['password'] = this.password;
    data['avatar'] = this.avatar;
    if (this.offer != null) {
      data['offer'] = this.offer.toJson();
    }
    return data;
  }
}