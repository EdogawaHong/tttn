import 'package:tttn/model/Chat.dart';

class ResChat {
  String key;
  Chat chat;

  ResChat({this.key, this.chat});

  ResChat.fromJson(Map<String, dynamic> json) {
    key = json['key'];
    chat = json['chat'] != null ? new Chat.fromJson(json['chat']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['key'] = this.key;
    if (this.chat != null) {
      data['chat'] = this.chat.toJson();
    }
    return data;
  }
}