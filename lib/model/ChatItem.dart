import 'package:tttn/model/Chat.dart';

class ChatItem {
  int idMessDetail;
  String content;
  String time;
  int idUser;

  ChatItem({this.idMessDetail, this.content, this.time, this.idUser});

  ChatItem.fromJson(Map<String, dynamic> json) {
    idMessDetail = json['id_mess_detail'];
    content = json['content'];
    time = json['time'];
    idUser = json['id_user'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_mess_detail'] = this.idMessDetail;
    data['content'] = this.content;
    data['time'] = this.time;
    data['id_user'] = this.idUser;
    return data;
  }
}