class UserInfo{
  int idUser;
  String name;
  String avatar;
  String phone;

  UserInfo({this.idUser, this.name, this.avatar,this.phone});

  UserInfo.fromJson(Map<String, dynamic> json) {
    idUser = json['id_user'];
    name = json['name'];
    avatar = json['avatar'];
    phone = json['phone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_user'] = this.idUser;
    data['name'] = this.name;
    data['avatar'] = this.avatar;
    data['phone'] = this.phone;
    return data;
  }
}