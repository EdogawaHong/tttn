import 'package:tttn/model/User.dart';

class ResUser {
  String key;
  User user;

  ResUser({this.key, this.user});

  ResUser.fromJson(Map<String, dynamic> json) {
    key = json['key'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['key'] = this.key;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}
