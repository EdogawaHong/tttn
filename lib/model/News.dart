import 'package:tttn/model/UserInfo.dart';

import 'Cmt.dart';

class News {
  int idNews;
  UserInfo user;
  int price;
  num area;
  String address;
  double lat;
  double lng;
  String desc;
  String time;
  int status;
  List<String> images;
  List<int> like;
  List<Cmt> cmt;

  News(
      {this.idNews,
        this.user,
        this.price,
        this.area,
        this.address,
        this.lat,
        this.lng,
        this.desc,
        this.time,
        this.status,
        this.images,
        this.like,
        this.cmt});

  News.fromJson(Map<String, dynamic> json) {
    idNews = json['id_news'];
    user = json['user'] != null ? new UserInfo.fromJson(json['user']) : null;
    price = json['price'];
    area = json['area'];
    address = json['address'];
    lat = json['lat'];
    lng = json['lng'];
    desc = json['desc'];
    time = json['time'];
    status = json['status'];
    images = json['images'].cast<String>();
    like = json['like'].cast<int>();
    if (json['cmt'] != null) {
      cmt = new List<Cmt>();
      json['cmt'].forEach((v) {
        cmt.add(new Cmt.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_news'] = this.idNews;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['price'] = this.price;
    data['area'] = this.area;
    data['address'] = this.address;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['desc'] = this.desc;
    data['time'] = this.time;
    data['status'] = this.status;
    data['images'] = this.images;
    data['like'] = this.like;
    if (this.cmt != null) {
      data['cmt'] = this.cmt.map((v) => v.toJson()).toList();
    }
    return data;
  }
}