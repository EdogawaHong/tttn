class Offer {
  int idOffer;
  String address;
  double area;
  int price;

  Offer({this.idOffer, this.address, this.area, this.price});

  Offer.fromJson(Map<String, dynamic> json) {
    idOffer = json['id_offer'];
    address = json['address'];
    area = json['area'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_offer'] = this.idOffer;
    data['address'] = this.address;
    data['area'] = this.area;
    data['price'] = this.price;
    return data;
  }
}