import 'package:tttn/model/News.dart';

class ResNews{
  String key;
  News news;

  ResNews({this.key, this.news});

  ResNews.fromJson(Map<String, dynamic> json) {
    key = json['key'];
    news = json['news'] != null ? new News.fromJson(json['news']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['key'] = this.key;
    if (this.news != null) {
      data['news'] = this.news.toJson();
    }
    return data;
  }
}