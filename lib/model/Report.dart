class Report {
  int idReport;
  String content;
  String time;
  String idNews;
  String idUser;

  Report({this.idReport, this.content, this.time, this.idNews, this.idUser});

  Report.fromJson(Map<String, dynamic> json) {
    idReport = json['id_report'];
    content = json['content'];
    time = json['time'];
    idNews = json['id_news'];
    idUser = json['id_user'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_report'] = this.idReport;
    data['content'] = this.content;
    data['time'] = this.time;
    data['id_news'] = this.idNews;
    data['id_user'] = this.idUser;
    return data;
  }
}