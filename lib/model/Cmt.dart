import 'UserInfo.dart';

class Cmt {
  int idCmt;
  String content;
  String time;
  UserInfo user;

  Cmt({this.idCmt, this.content, this.time, this.user});

  Cmt.fromJson(Map<String, dynamic> json) {
    idCmt = json['id_cmt'];
    content = json['content'];
    time = json['time'];
    user = json['user'] != null ? new UserInfo.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_cmt'] = this.idCmt;
    data['content'] = this.content;
    data['time'] = this.time;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}