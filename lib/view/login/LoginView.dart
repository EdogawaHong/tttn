import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:tttn/commons/AppConstant.dart';
import 'package:tttn/commons/Constance.dart';
import 'package:tttn/commons/SharedPref.dart';
import 'package:tttn/commons/Utils.dart';
import 'package:tttn/controllers/UserInf.dart';
import 'package:tttn/controllers/UserPresenter.dart';
import 'package:tttn/model/ResUser.dart';
import 'package:tttn/view/home/HomeView.dart';
import 'package:tttn/view/register/Register.dart';

class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> implements UserInf{
  TextEditingController phoneController = TextEditingController();
  TextEditingController passController = TextEditingController();

  UserPresenter presenter;
  List<ResUser> listUsers=List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    presenter=UserPresenter(this);
    //presenter.getListUsers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body:Stack(
          children: [
            Container(
              width: double.infinity,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      colors: [
                        Colors.yellow[900],
                        Colors.yellow[700],
                        Colors.yellow[400]
                      ]
                  )
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 80,),
                  Padding(
                    padding: EdgeInsets.all(15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Đăng nhập',style: TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.w500,
                          color: Colors.white,
                        ),),
                        //SizedBox(height: 10,),
                        Text('Tiện lợi và hữu ích',style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                        ),)
                      ],
                    ),
                  ),
                  Expanded(child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(60),topRight: Radius.circular(60))
                    ),
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: EdgeInsets.all(20),
                        child: Column(
                          children: [
                            SizedBox(height: 40,),
                            Card(
                              elevation: 5,
                              shadowColor: Colors.yellow,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  children: [
                                    TextField(
                                      style: AppConstant.inputStyle,
                                      controller: phoneController,
                                      decoration: InputDecoration(
                                        hintText: 'Phone number',
                                        hintStyle: AppConstant.hintStyle,
                                        border: InputBorder.none,
                                        labelText: 'Phone number',
                                        //labelStyle: AppConstant.labelStyle
                                      ),
                                    ),
                                    Container(height: 0.5,color: Colors.grey,
                                      //margin: EdgeInsets.symmetric(vertical: 8),
                                    ),
                                    TextField(
                                      style: AppConstant.inputStyle,
                                      obscureText: true,
                                      keyboardType: TextInputType.visiblePassword,
                                      controller: passController,
                                      decoration: InputDecoration(
                                          hintText: 'Password',
                                          hintStyle: AppConstant.hintStyle,
                                          border: InputBorder.none,
                                          labelText: 'Password'
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(height: 25,),
                            GestureDetector(
                              onTap: (){
                                presenter.getListUsers();
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(25),
                                    color: AppConstant.colorPrimary
                                ),
                                child: Text('Đăng nhập',style: AppConstant.actionStyle,),
                                alignment: Alignment.center,
                                padding: EdgeInsets.all(8),
                              ),
                            ),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'Bạn chưa có tài khoản? ',
                                    style: AppConstant.hintStyle.copyWith(fontSize: 15),
                                  ),
                                  InkWell(
                                    onTap: (){
                                      Utils.moveAndReplaceToScreen(Register(), context);
                                    },
                                    child: Text(
                                      'Tạo tài khoản mới',
                                      style: AppConstant.labelStyle.copyWith(fontSize: 15),
                                    ),
                                  )
                                ],
                              ),
                              padding: EdgeInsets.symmetric(vertical: 30),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ))
                ],
              ),
            ),
            (progress)
                ? Utils.CircularProgress(context)
                : Container()
          ],
        )
    );
  }

  @override
  Future<void> loginSuccess(ResUser resUser) async {
    if(resUser!=null){
      await SharedPref.save(Constance.user_storage, resUser);
      Utils.showToast("Đăng nhập thành công!");
      Utils.moveToScreen(HomeView(),context);
    }
  }
  bool progress=false;

  @override
  void showProgress(bool b) {
    setState(() {
      progress=b;
    });
  }

  @override
  void getListUser(List<ResUser> listUsers) {
    print("login get list user: ${listUsers.length}");
    if(listUsers!=null ||listUsers.length>0){
      setState(() {
        this.listUsers.addAll(listUsers);
        presenter.checkAccount(phoneController.text.trim(), passController.text.trim(),listUsers);
      });
    }
  }

  @override
  void registerSuccess(bool b) {
    // TODO: implement registerSuccess
  }
}

