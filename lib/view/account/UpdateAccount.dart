import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tttn/commons/AppConstant.dart';
import 'package:tttn/commons/Constance.dart';
import 'package:tttn/commons/SharedPref.dart';
import 'package:tttn/commons/Utils.dart';
import 'package:tttn/controllers/AccountInf.dart';
import 'package:tttn/controllers/AccountPresenter.dart';
import 'package:tttn/model/ResUser.dart';
import 'package:path/path.dart';
import 'package:tttn/model/User.dart';

class UpdateAccount extends StatefulWidget {
  final ResUser resUser;

  const UpdateAccount({Key key, this.resUser}) : super(key: key);

  @override
  _UpdateAccountState createState() => _UpdateAccountState();
}

class _UpdateAccountState extends State<UpdateAccount> implements AccountInf{
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passController = TextEditingController();

  bool showPass = false;
  bool progress=false;
  
  AccountPresenter presenter;

  @override
  void initState() {
    super.initState();
    presenter=AccountPresenter(this);
    nameController = TextEditingController(text: '${widget.resUser.user.name}');
    emailController =
        TextEditingController(text: '${widget.resUser.user.email}');
    passController =
        TextEditingController(text: '${widget.resUser.user.password}');
    image = widget.resUser.user.avatar;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Thông tin tài khoản',
          style: TextStyle(fontSize: 22),
        ),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Center(
              child: Container(
                width: Get.width - 32,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(8.0),
                      width: 116,
                      height: 116,
                      child: Stack(
                        children: [
                          CircleAvatar(
                              radius: 50,
                              backgroundColor: AppConstant.colorPrimary,
                              child: ClipOval(
                                child: (image == null || image == '')
                                    ? Icon(
                                        Icons.account_circle,
                                        size: 100,
                                        color: Colors.white,
                                      )
                                    : Image.network(
                                        image,
                                        width: 100,
                                        height: 100,
                                        fit: BoxFit.cover,
                                      ),
                              )),
                          Container(
                              height: 100,
                              alignment: Alignment.bottomRight,
                              child: IconButton(
                                icon: Icon(
                                  Icons.camera_alt,
                                  size: 50,
                                  color: Colors.grey,
                                ),
                                onPressed: () {
                                  _showChoiceDialog();
                                },
                                alignment: Alignment.bottomRight,
                              ))
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 32,
                    ),
                    Row(
                      children: [
                        Text(
                          'Tên tài khoản:',
                          style: AppConstant.inputStyle,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Expanded(
                            child: TextField(
                          controller: nameController,
                          style: AppConstant.inputStyle,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Nhập tên tài khoản*',
                            hintStyle: AppConstant.infoStyle,
                            isDense: true,
                            contentPadding: EdgeInsets.all(0),
                          ),
                        ))
                      ],
                    ),
                    Utils.borderBottom(),
                    Row(
                      children: [
                        Text(
                          'Số điện thoại:',
                          style: AppConstant.inputStyle,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Expanded(
                            child: Text(
                          "${widget.resUser.user.phone}",
                          style: AppConstant.infoStyle,
                        ))
                      ],
                    ),
                    Utils.borderBottom(),
                    Row(
                      children: [
                        Text(
                          'Email:',
                          style: AppConstant.inputStyle,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Expanded(
                            child: TextField(
                          controller: emailController,
                          keyboardType: TextInputType.emailAddress,
                          style: AppConstant.inputStyle,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Nhập email',
                            hintStyle: AppConstant.infoStyle,
                            isDense: true,
                            contentPadding: EdgeInsets.all(0),
                          ),
                        ))
                      ],
                    ),
                    Utils.borderBottom(),
                    Row(
                      children: [
                        Text(
                          'Mật khẩu:',
                          style: AppConstant.inputStyle,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Expanded(
                            child: TextField(
                          controller: passController,
                          style: AppConstant.inputStyle,
                          keyboardType: TextInputType.visiblePassword,
                          obscureText: showPass,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Nhập mật khẩu',
                            hintStyle: AppConstant.infoStyle,
                            isDense: true,
                            contentPadding: EdgeInsets.all(0),
                          ),
                        )),
                        InkWell(
                            onTap: () {
                              setState(() {
                                showPass = !showPass;
                              });
                            },
                            child: Icon(
                              Icons.remove_red_eye,
                              size: 25,
                              color: (showPass)
                                  ? AppConstant.colorPrimary
                                  : Colors.grey,
                            )),
                        SizedBox(
                          width: 5,
                        ),
                      ],
                    ),
                    Utils.borderBottom(),
                    SizedBox(
                      height: 50,
                    ),
                    InkWell(
                      onTap: () {
                        updateAccount();
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 32, vertical: 12),
                        decoration: BoxDecoration(
                            color: AppConstant.colorPrimary,
                            borderRadius: BorderRadius.all(Radius.circular(30))),
                        child: Text(
                          'Cập nhật',
                          style: AppConstant.actionStyle,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          (progress)?Utils.CircularProgress(context):Container()
        ],
      ),
    );
  }

  final picker = ImagePicker();
  bool load = false;
  String image;

  Future<void> _showChoiceDialog() {
    return Get.dialog(AlertDialog(
      title: Text('Chọn nguồn ảnh!'),
      content: SingleChildScrollView(
        child: ListBody(
          children: [
            GestureDetector(
              child: Row(
                children: [
                  Icon(
                    Icons.insert_photo,
                    size: 24,
                    color: AppConstant.colorPrimary,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Text('Thư viện'),
                ],
              ),
              onTap: () {
                Get.back();
                _openGallery();
              },
            ),
            Padding(padding: EdgeInsets.all(8)),
            GestureDetector(
              child: Row(
                children: [
                  Icon(
                    Icons.camera_alt,
                    size: 24,
                    color: AppConstant.colorPrimary,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Text('Chụp ảnh'),
                ],
              ),
              onTap: () {
                Get.back();
                _openCamera();
              },
            ),
          ],
        ),
      ),
    ));
  }

  _openCamera() async {
    final pickedFile =
        await picker.getImage(source: ImageSource.camera, imageQuality: 20);
    if (pickedFile != null) {
      File _image = File(pickedFile.path);
      print('image size: ${_image.lengthSync() / (1024)}');
      await uploadImage(_image);
    }
    setState(() {
      load = false;
    });
  }

  _openGallery() async {
    var pickedFile = await picker.getImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      File _image = File(pickedFile.path);
      print('image size: ${_image.lengthSync() / (1024)}');
      await uploadImage(_image);
    }
    setState(() {
      load = false;
    });
  }

  Future uploadImage(File imageFile) async {
    setState(() {
      load = true;
    });
    var file = File(imageFile.path);
    String filename = basename(imageFile.path);
    final _storage = FirebaseStorage.instance;
    var sna = await _storage.ref().child(filename).putFile(file);
    var downloadUrl = await sna.ref.getDownloadURL();
    print("uploadedFileURL: $downloadUrl");
    if (downloadUrl != null) {
      setState(() {
        image = downloadUrl;
      });
    }
  }
  
  checkNull(){
    if(nameController.text.trim().isEmpty||passController.text.trim().isEmpty){
      Utils.showToast("Tên tài khoản và mật khẩu không được để trống!");
      return false;
    }
    if(emailController.text.trim().isNotEmpty){
      if(!GetUtils.isEmail(emailController.text.trim())){
        Utils.showToast("Email không hợp lệ!");
        return false;
      }
    }
    return true;
  }

  updateAccount(){
    if(checkNull()){
      setState(() {
        progress=true;
      });
      ResUser accountUpdate=ResUser(
        key: widget.resUser.key,
        user: User(
          idUser: widget.resUser.user.idUser,
          name: nameController.text.trim(),
          phone: widget.resUser.user.phone,
          email: emailController.text.trim(),
          password: passController.text.trim(),
          avatar: image,
          offer: widget.resUser.user.offer
        )
      );
      presenter.updateAccount(accountUpdate);
    }
  }

  @override
  void getInfoAccount(ResUser resUser) {
      print('user update: ${resUser.toJson()}');
  }

  @override
  Future<void> updateAccountStatus(bool b,ResUser resUser) async {
    setState(() {
      progress=false;
    });
      if(b){
        await SharedPref.save(Constance.user_storage, resUser);
        presenter.loadUserInfo();
        Utils.showToast("Cập nhật thành công!");
      }else{
        Utils.showToast("Có lỗi xảy ra! Vui lòng thử lại sau!");
      }
  }
}
