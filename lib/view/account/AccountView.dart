import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:tttn/commons/AppConstant.dart';
import 'package:tttn/commons/Constance.dart';
import 'package:tttn/commons/SharedPref.dart';
import 'package:tttn/controllers/AccountPresenter.dart';
import 'package:tttn/controllers/AccountInf.dart';
import 'package:tttn/model/ResUser.dart';
import 'package:tttn/model/UserInfo.dart';
import 'package:tttn/view/account/ProfileView.dart';
import 'package:tttn/view/account/UpdateAccount.dart';
import 'package:tttn/view/login/LoginView.dart';

class AccountView extends StatefulWidget {
  @override
  _AccountViewState createState() => _AccountViewState();
}

class _AccountViewState extends State<AccountView> implements AccountInf {
  AccountPresenter presenter;
  ResUser resUser;

  @override
  void initState() {
    super.initState();
    presenter = AccountPresenter(this);
    presenter.loadUserInfo();
  }

  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 200,
          alignment: Alignment.center,
          child: (resUser != null)
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: CircleAvatar(
                        radius: 50,
                        backgroundColor: AppConstant.colorPrimary,
                        child: ClipOval(
                          child: (resUser.user.avatar == null ||
                                  resUser.user.avatar == '')
                              ? Icon(
                                  Icons.account_circle,
                                  size: 100,
                                  color: Colors.white,
                                )
                              : Image.network(
                                  resUser.user.avatar,
                                  width: 100,
                                  height: 100,
                                  fit: BoxFit.cover,
                                ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Text(
                      "${resUser.user.name}",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 25,
                          fontWeight: FontWeight.w500),
                    ),
                  ],
                )
              : Container(),
        ),
        SizedBox(
          height: 32,
        ),
        Column(
          children: [
            InkWell(
                onTap: () {
                  Get.to(UpdateAccount(
                    resUser: resUser,
                  )).then((value) => presenter.loadUserInfo());
                },
                child: action(Icons.edit, 'Cập nhật tài khoản')),
            InkWell(
                onTap: () {
                  UserInfo userInfo = UserInfo(
                      idUser: resUser.user.idUser,
                      name: resUser.user.name,
                      avatar: resUser.user.avatar,
                      phone: resUser.user.phone);
                  Get.to(ProfileView(
                    account: userInfo,
                    user: userInfo,
                  ));
                },
                child: action(Icons.list, 'Danh sách bài viết')),
            InkWell(
                onTap: () {}, child: action(Icons.today, 'Đề xuất thông tin')),
            InkWell(
                onTap: () {
                  dialogLogout();
                },
                child: action(Icons.logout, 'Đăng xuất')),
          ],
        )
      ],
    );
  }

  Widget action(IconData icon, String action) {
    return Container(
      decoration: BoxDecoration(
        color: AppConstant.colorPrimary,
        borderRadius: BorderRadius.all(Radius.circular(15)),
      ),
      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
      margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            icon,
            color: Colors.white,
            size: 30,
          ),
          SizedBox(
            width: 8,
          ),
          Text(
            '$action',
            style: AppConstant.textStyle,
          ),
        ],
      ),
    );
  }

  dialogLogout() {
    Get.defaultDialog(
      title: 'Xác nhận',
      middleText: 'Bạn có muốn đăng xuất khỏi hệ thống?',
      middleTextStyle: AppConstant.textStyle,
      titleStyle: AppConstant.labelStyle.copyWith(fontSize: 30),
      confirm: InkWell(
        onTap: () {
          SharedPref.clear().whenComplete(() {
            Get.offAll(LoginView());
          });
        },
        child: Container(
          width: 150,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: AppConstant.colorPrimary,
            borderRadius: BorderRadius.all(Radius.circular(20)),
          ),
          padding: EdgeInsets.all(8),
          child: Text(
            'Đăng xuất',
            style: TextStyle(fontSize: 22, color: Colors.white),
          ),
        ),
      ),
      cancel: InkWell(
        onTap: () {
          Get.back();
        },
        child: Container(
          width: 100,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            border: Border.all(color: AppConstant.colorPrimary, width: 1),
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
          ),
          padding: EdgeInsets.all(8),
          child: Text(
            'Hủy',
            style: TextStyle(fontSize: 22, color: Colors.black),
          ),
        ),
      ),
    );
  }

  @override
  void getInfoAccount(ResUser resUser) {
    if (resUser != null) {
      setState(() {
        this.resUser = resUser;
      });
    }
  }

  @override
  void updateAccountStatus(bool b, ResUser resUser) {
    // TODO: implement updateAccount
  }
}
