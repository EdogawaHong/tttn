import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:tttn/commons/AppConstant.dart';
import 'package:tttn/commons/Constance.dart';
import 'package:tttn/commons/Utils.dart';
import 'package:tttn/controllers/ProfileInf.dart';
import 'package:tttn/controllers/ProfilePresenter.dart';
import 'package:tttn/controllers/UpdateNewsInf.dart';
import 'package:tttn/controllers/UpdateNewsPresenter.dart';
import 'package:tttn/model/Cmt.dart';
import 'package:tttn/model/News.dart';
import 'package:tttn/model/ResNews.dart';
import 'package:tttn/model/ResUser.dart';
import 'package:tttn/model/UserInfo.dart';
import 'package:tttn/view/chat/DetailChatView.dart';
import 'package:tttn/view/news/AddNews.dart';
import 'package:tttn/view/news/DetailNews.dart';
import 'package:tttn/view/news/DialogReport.dart';
import 'package:tttn/view/news/LikeView.dart';
import 'package:tttn/view/news/NewsItem.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfileView extends StatefulWidget {
  final UserInfo account;
  final UserInfo user;

  const ProfileView({Key key, this.account, this.user}) : super(key: key);

  @override
  _ProfileViewState createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> implements UpdateNewsInf,ProfileInf {
  UpdateNewsPresenter presenter;
  ProfilePresenter _profilePresenter;

  bool b = false;
  bool showLoading = false;

  @override
  void initState() {
    super.initState();
    _profilePresenter=ProfilePresenter(this);
    _profilePresenter.getNews(widget.user.idUser);
    presenter = UpdateNewsPresenter(this);
    //getListNews();
    (widget.account.idUser == widget.user.idUser) ? b = true : b = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          (widget.account.idUser == widget.user.idUser)
              ? 'Danh sách bài viết'
              : '${widget.user.name}',
          style: TextStyle(fontSize: 22),
        ),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                infoUser(),
                (listNews != null && listNews.length > 0)
                    ? ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: listNews.length,
                        itemBuilder: (BuildContext context, int index) =>
                            getItem(context, listNews[index], index))
                    : Utils.getEmpty(context),
              ],
            ),
          ),
          (showLoading)?Utils.CircularProgress(context):Container()
        ],
      ),
      floatingActionButton: (b)
          ? FloatingActionButton(
              onPressed: () {
                Get.to(AddNews(
                  id: 0,
                  resNews: null,
                ));
              },
              child: Icon(
                Icons.add,
                size: 35,
              ))
          : Container(),
    );
  }

  infoUser() {
    return (widget.account.idUser != widget.user.idUser)
        ? Card(
            elevation: 5,
            shadowColor: AppConstant.colorPrimary,
            margin: EdgeInsets.symmetric(horizontal: 8),
            child: Container(
              width: Get.width,
              padding: EdgeInsets.all(8),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CircleAvatar(
                          radius: 35,
                          backgroundColor: AppConstant.colorPrimary,
                          child: ClipOval(
                            child: (widget.user.avatar == null ||
                                    widget.user.avatar == '')
                                ? Icon(
                                    Icons.account_circle,
                                    size: 70,
                                    color: Colors.white,
                                  )
                                : Image.network(
                                    widget.user.avatar,
                                    width: 70,
                                    height: 70,
                                    fit: BoxFit.cover,
                                  ),
                          ),
                        ),
                        SizedBox(
                          height: 0,
                        ),
                        Text(
                          "${widget.user.name}",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 25,
                              fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      InkWell(
                        onTap: () async {
                          await launch("tel:${widget.user.phone}");
                        },
                        child: Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                              borderRadius:
                              BorderRadius.all(Radius.circular(30)),
                              color: AppConstant.colorPrimary),
                          child: Icon(
                            Icons.phone,
                            color: Colors.white,
                            size: 26,
                          ),
                        ),
                      ),
                      SizedBox(width: 16,),
                      InkWell(
                        onTap: () {
                          Get.to(DetailChatView(user1: widget.account,user2: widget.user,));
                        },
                        child: Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                              borderRadius:
                              BorderRadius.all(Radius.circular(30)),
                              color: AppConstant.colorPrimary),
                          child: Icon(
                            Icons.message,
                            color: Colors.white,
                            size: 26,
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          )
        : Container();
  }

  List<ResNews> listNews = List();

  TextEditingController reportController=TextEditingController();

  getItem(BuildContext context, ResNews resNews, int index) {
    print("news: ${resNews.news.toJson()}");
    List<String> imageUrls = resNews.news.images;
    bool status=false;
    if(resNews.news.like!=null &&resNews.news.like.length>0){
      resNews.news.like.forEach((e) {
        if(e==widget.account.idUser) {
          status=true;
          return;
        }
      });
    }
    return Card(
      elevation: 4,
      shadowColor: AppConstant.colorPrimary,
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
      child: Container(
        color: Colors.white,
        padding: EdgeInsets.all(8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    CircleAvatar(
                      backgroundColor: AppConstant.colorPrimary,
                      radius: 16,
                      child: ClipOval(
                          child: (resNews.news.user.avatar != null &&
                                  resNews.news.user.avatar != '')
                              ? Image.network(
                                  resNews.news.user.avatar,
                                  fit: BoxFit.cover,
                                  width: 32,
                                  height: 32,
                                )
                              : Icon(
                                  Icons.account_circle,
                                  color: Colors.white,
                                  size: 32,
                                )),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '${resNews.news.user.name}',
                          style: AppConstant.inputStyle
                              .copyWith(fontWeight: FontWeight.w500),
                        ),
                        Text(
                          '${resNews.news.time}',
                          style: AppConstant.timeStyle,
                        ),
                      ],
                    )
                  ],
                ),
                (widget.account.idUser == widget.user.idUser)
                    ? PopupMenuButton(
                        itemBuilder: (BuildContext context) => [
                          PopupMenuItem(
                              child: Text(
                                'Xóa bài viết',
                                style: AppConstant.inputStyle,
                              ),
                              value: 0),
                          PopupMenuItem(
                            child: Text(
                              'Chỉnh sửa bài viết',
                              style: AppConstant.inputStyle,
                            ),
                            value: 1,
                          ),
                        ],
                        onSelected: (value) {
                          print('$value');
                          if (value == 0) {
                            dialogRemove(resNews.key);
                          }
                          if (value == 1) {
                            Get.to(AddNews(
                              id: 0,
                              resNews: resNews,
                            ));
                          }
                        },
                      )
                    : PopupMenuButton(
                        itemBuilder: (BuildContext context) => [
                          PopupMenuItem(
                              child: Text(
                                'Báo cáo',
                                style: AppConstant.inputStyle,
                              ),
                              value: 0),
                        ],
                        onSelected: (value) {
                          print('$value');
                          if(value==0){
                            DialogReport.dialogReport(reportController,resNews.key);
                          }
                        },
                      )
              ],
            ),
            SizedBox(
              height: 12,
            ),
            Text(
              'Diện tích: ${resNews.news.area} m2',
              style: AppConstant.infoStyle,
            ),
            SizedBox(
              height: 4,
            ),
            Text(
              'Giá: ${Utils.formatMoney(resNews.news.price)} VND/tháng',
              style: AppConstant.infoStyle,
            ),
            SizedBox(
              height: 4,
            ),
            Text(
              'Địa chỉ: ${resNews.news.address}',
              style: AppConstant.infoStyle,
            ),
            SizedBox(
              height: 4,
            ),
            (resNews.news.desc != null || resNews.news.desc == '')
                ? Text(
                    'Mô tả: ${resNews.news.desc}',
                    style: AppConstant.infoStyle,
                  )
                : Container(),
            SizedBox(
              height: 4,
            ),
            (imageUrls != null && imageUrls.length > 0)
                ? GridView.extent(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    maxCrossAxisExtent: 150,
                    mainAxisSpacing: 4.0,
                    crossAxisSpacing: 4.0,
                    children: imageUrls.map((e) {
                      return NewsItem.setImageUrl(e);
                    }).toList())
                : Container(),
            SizedBox(
              height: 6,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                (resNews.news.like == null || resNews.news.like.length == 0)
                    ? Container()
                    : InkWell(
                  onTap: () {
                    Utils.moveToScreen(
                        LikeView(
                          likes: resNews.news.like,
                        ),
                        context);
                  },
                      child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(CupertinoIcons.heart_fill,
                                size: 20, color: AppConstant.colorPrimary),
                            SizedBox(
                              width: 2,
                            ),
                            Text(
                              '${resNews.news.like.length}',
                              style:
                                  TextStyle(fontSize: 16, color: Colors.black54),
                            )
                          ],
                        ),
                    ),
                Text(
                  '${(resNews.news.cmt == null) ? 0 : resNews.news.cmt.length} bình luận',
                  style: TextStyle(fontSize: 16, color: Colors.black54),
                )
              ],
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 6),
              height: 0.5,
              color: Colors.grey,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: () {
                    setState(() {
                      status = !status;
                      presenter.updateStatusLike(
                          widget.account.idUser, resNews);
                    });
                  },
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        CupertinoIcons.heart_fill,
                        size: 25,
                        color: (status)
                            ? AppConstant.colorPrimary
                            : Colors.grey,
                      ),
                      SizedBox(
                        width: 4,
                      ),
                      Text(
                        'Thích',
                        style: TextStyle(fontSize: 18, color: Colors.black87),
                      )
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    //Utils.moveToScreen(DetailNews(keyDB: resNews.key),context);
                    Get.to(DetailNews(keyDB: resNews.key))
                        .then((value) => _profilePresenter.getNews(widget.user.idUser));
                  },
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.speaker_notes_outlined,
                        size: 25,
                        color: AppConstant.colorPrimary,
                      ),
                      SizedBox(
                        width: 4,
                      ),
                      Text(
                        'Bình luận',
                        style: TextStyle(fontSize: 18, color: Colors.black87),
                      )
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  dialogRemove(String key) {
    Get.defaultDialog(
      title: 'Xác nhận',
      middleText: 'Bạn có muốn xóa bài viết này không?',
      middleTextStyle: AppConstant.textStyle,
      titleStyle: AppConstant.labelStyle.copyWith(fontSize: 30),
      confirm: InkWell(
        onTap: () {
          FirebaseDatabase.instance
              .reference()
              .child(Constance.news)
              .child(key)
              .remove();
          _profilePresenter.getNews(widget.user.idUser);
          Get.back();
        },
        child: Container(
          width: 150,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: AppConstant.colorPrimary,
            borderRadius: BorderRadius.all(Radius.circular(20)),
          ),
          padding: EdgeInsets.all(8),
          child: Text(
            'Đồng ý',
            style: TextStyle(fontSize: 22, color: Colors.white),
          ),
        ),
      ),
      cancel: InkWell(
        onTap: () {
          Get.back();
        },
        child: Container(
          width: 100,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            border: Border.all(color: AppConstant.colorPrimary, width: 1),
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
          ),
          padding: EdgeInsets.all(8),
          child: Text(
            'Hủy',
            style: TextStyle(fontSize: 22, color: Colors.black),
          ),
        ),
      ),
    );
  }

  @override
  void updateNewsSuccess(bool b, ResNews resNews) {
    // TODO: implement updateNewsSuccess
  }

  @override
  void getNews(List<ResNews> data) {
    // TODO: implement getNews
    setState(() {
      listNews.clear();
      if(data!=null && data.length>0){
        listNews.addAll(data);
      }
    });
  }

  @override
  void showProgress(bool b) {
    // TODO: implement showProgress
    setState(() {
      showLoading=b;
    });
  }
}
