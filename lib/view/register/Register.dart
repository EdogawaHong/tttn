import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:tttn/commons/AppConstant.dart';
import 'package:tttn/commons/Constance.dart';
import 'package:tttn/commons/SharedPref.dart';
import 'package:tttn/commons/Utils.dart';
import 'package:tttn/controllers/UserInf.dart';
import 'package:tttn/controllers/UserPresenter.dart';
import 'package:tttn/model/ResUser.dart';
import 'package:tttn/model/User.dart';
import 'package:tttn/view/home/HomeView.dart';
import 'package:tttn/view/login/LoginView.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> implements UserInf{
  TextEditingController phoneController = TextEditingController();
  TextEditingController passController = TextEditingController();
  TextEditingController repeatController = TextEditingController();

  UserPresenter presenter;

  @override
  void initState() {
    super.initState();
    presenter=UserPresenter(this);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            decoration: BoxDecoration(
                gradient: LinearGradient(begin: Alignment.topCenter, colors: [
                  Colors.yellow[900],
                  Colors.yellow[700],
                  Colors.yellow[400]
                ])),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 80,
                ),
                Padding(
                  padding: EdgeInsets.all(15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Đăng ký',
                        style: TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.w500,
                          color: Colors.white,
                        ),
                      ),
                      //SizedBox(height: 10,),
                      Text(
                        'Nhanh chóng và dễ dàng',
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
                Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(60),
                              topRight: Radius.circular(60))),
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: EdgeInsets.all(20),
                          child: Column(
                            children: [
                              SizedBox(
                                height: 40,
                              ),
                              Card(
                                elevation: 5,
                                shadowColor: Colors.yellow,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    children: [
                                      TextField(
                                        style: AppConstant.inputStyle,
                                        controller: phoneController,
                                        decoration: InputDecoration(
                                          hintText: 'Phone number',
                                          hintStyle: AppConstant.hintStyle,
                                          border: InputBorder.none,
                                          labelText: 'Phone number',
                                          //labelStyle: AppConstant.labelStyle
                                        ),
                                      ),
                                      Container(
                                        height: 0.5,
                                        color: Colors.grey,
                                      ),
                                      TextField(
                                        style: AppConstant.inputStyle,
                                        controller: passController,
                                        obscureText: true,
                                        keyboardType: TextInputType.visiblePassword,
                                        decoration: InputDecoration(
                                            hintText: 'Password',
                                            hintStyle: AppConstant.hintStyle,
                                            border: InputBorder.none,
                                            labelText: 'Password'),
                                      ),
                                      Container(
                                        height: 0.5,
                                        color: Colors.grey,
                                      ),
                                      TextField(
                                        controller: repeatController,
                                        style: AppConstant.inputStyle,
                                        obscureText: true,
                                        keyboardType: TextInputType.visiblePassword,
                                        decoration: InputDecoration(
                                            hintText: 'Repeat password',
                                            hintStyle: AppConstant.hintStyle,
                                            border: InputBorder.none,
                                            labelText: 'Repeat password'),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 25,
                              ),
                              GestureDetector(
                                onTap: () {
                                  presenter.getListUsers();
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(25),
                                      color: AppConstant.colorPrimary),
                                  child: Text(
                                    'Đăng ký',
                                    style: AppConstant.actionStyle,
                                  ),
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.all(8),
                                ),
                              ),
                              Container(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Bạn đã có tài khoản? ',
                                      style:
                                      AppConstant.hintStyle.copyWith(fontSize: 15),
                                    ),
                                    InkWell(
                                      onTap: () {
                                        Utils.moveAndReplaceToScreen(
                                            LoginView(), context);
                                      },
                                      child: Text(
                                        'Đăng nhập',
                                        style: AppConstant.labelStyle
                                            .copyWith(fontSize: 15),
                                      ),
                                    )
                                  ],
                                ),
                                padding: EdgeInsets.symmetric(vertical: 30),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ))
              ],
            ),
          ),
          (progress)?Utils.CircularProgress(context):Container()
        ],
      ),
    );
  }

  bool progress=false;
  @override
  Future<void> loginSuccess(ResUser resUser) async {
    if(resUser!=null){
      await SharedPref.save(Constance.user_storage, resUser);
      Utils.showToast("Đăng ký thành công!");
      Get.off(HomeView());
    }
  }

  @override
  void showProgress(bool b) {
      setState(() {
        progress=b;
      });
  }

  List<ResUser> listUsers=List();
  int d=0;
  @override
  void getListUser(List<ResUser> listUsers) {
    if(listUsers!=null) {
      if(d==0)
      presenter.register(
          phoneController.text.trim(),
          passController.text.trim(),
          repeatController.text.trim(), listUsers);
      else presenter.checkAccount(phoneController.text.trim(),
          passController.text.trim(), listUsers);
    }
  }

  @override
  void registerSuccess(bool b) {
    if(b){
      presenter.getListUsers();
      setState(() {
        d++;
      });
    }
  }
}

