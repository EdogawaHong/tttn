import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:tiengviet/tiengviet.dart';
import 'package:tttn/commons/AppConstant.dart';
import 'package:tttn/commons/Constance.dart';
import 'package:tttn/commons/Utils.dart';
import 'package:tttn/controllers/AccountInf.dart';
import 'package:tttn/controllers/AccountPresenter.dart';
import 'package:tttn/controllers/NewsInf.dart';
import 'package:tttn/controllers/NewsPresenter.dart';
import 'package:tttn/controllers/UpdateNewsInf.dart';
import 'package:tttn/controllers/UpdateNewsPresenter.dart';
import 'package:tttn/model/Cmt.dart';
import 'package:tttn/model/News.dart';
import 'package:tttn/model/ResNews.dart';
import 'package:tttn/model/ResUser.dart';
import 'package:tttn/model/User.dart';
import 'package:tttn/model/UserInfo.dart';
import 'package:tttn/view/account/ProfileView.dart';
import 'package:tttn/view/news/NewsItem.dart';
import 'package:tttn/view/news/LikeView.dart';

import 'AddNews.dart';
import 'DetailNews.dart';
import 'DialogReport.dart';

class NewsView extends StatefulWidget {
  @override
  _NewsViewState createState() => _NewsViewState();
}

class _NewsViewState extends State<NewsView>
    implements UpdateNewsInf, AccountInf,NewsInf {
  DatabaseReference _reference;
  int length;
  UpdateNewsPresenter updatePresenter;
  AccountPresenter accountPresenter;
  NewsPresenter newsPresenter;

  TextEditingController findController = TextEditingController();

  bool showLoading=false;

  @override
  void initState() {
    super.initState();
    _reference = FirebaseDatabase.instance.reference().child(Constance.news);
    length = 0;
    //getListNews();
    newsPresenter=NewsPresenter(this);
    updatePresenter = UpdateNewsPresenter(this);
    accountPresenter = AccountPresenter(this);
    accountPresenter.loadUserInfo();
    newsPresenter.getNews();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.search),
        title: Container(
          decoration: BoxDecoration(
              border:
                  Border(bottom: BorderSide(color: Colors.white, width: 1))),
          child: TextField(
            controller: findController,
            cursorColor: Colors.white,
            onChanged: (value) {
              setState(() {
                listNews.clear();
                if (value != null) {
                  tempNews.map((e) {
                    if (TiengViet.parse(e.news.address)
                            .toUpperCase()
                            .toString()
                            .contains(
                                TiengViet.parse(value.toUpperCase().trim())) ||
                        TiengViet.parse(e.news.user.name)
                            .toUpperCase()
                            .toString()
                            .contains(
                                TiengViet.parse(value.toUpperCase().trim())))
                      setState(() {
                        listNews.add(e);
                      });
                  }).toList();
                } else {
                  setState(() {
                    listNews.addAll(tempNews);
                  });
                }
              });
            },
            style: TextStyle(color: Colors.white, fontSize: 18),
            decoration: InputDecoration(
                hintText: "Tìm kiếm...",
                focusColor: Colors.white,
                hintStyle: TextStyle(color: Colors.white54, fontSize: 18),
                isDense: true,
                contentPadding: EdgeInsets.symmetric(vertical: 8),
                border: InputBorder.none),
          ),
        ),
      ),
      body: Stack(
        children: [
          (listNews != null && listNews.length > 0)
              ? ListView.builder(
              shrinkWrap: true,
              itemCount: listNews.length,
              itemBuilder: (BuildContext context, int index) =>
                  getItem(context, listNews[index], index))
              : Utils.getEmpty(context),
          (showLoading)?Utils.CircularProgress(context):Container()
        ],
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Get.to(AddNews(
              id: length,
              resNews: null,
            ));
          },
          child: Icon(
            Icons.add,
            size: 35,
          )),
    );
  }

  List<ResNews> listNews = List();
  List<ResNews> tempNews = List();

  getListNews() {
    DatabaseReference _reference =
        FirebaseDatabase.instance.reference().child(Constance.news);
    listNews.clear();
    _reference.once().then((DataSnapshot snapshot) {
      print(snapshot.value.entries.length);
      for (var val in snapshot.value.entries) {
        print(val.value.toString());
        print(snapshot.key);
        print("val.key: ${val.key}");
        UserInfo userInfo;
        if (val.value[Constance.user] != null) {
          userInfo = UserInfo(
            idUser: val.value[Constance.user][Constance.idUser],
            name: val.value[Constance.user][Constance.name],
            avatar: val.value[Constance.user][Constance.avatar],
            phone: val.value[Constance.user][Constance.phone],
          );
        }
        List<dynamic> images = val.value[Constance.images];
        List<dynamic> likes = val.value[Constance.like];
        List<dynamic> resCmts = val.value[Constance.cmt];
        List<Cmt> cmts = List();
        if (resCmts != null && resCmts.length > 0) {
          for (int i = 0; i < resCmts.length; i++) {
            Cmt cmt = Cmt(
                idCmt: resCmts[i][Constance.idCcmt],
                content: resCmts[i][Constance.content],
                time: resCmts[i][Constance.time],
                user: UserInfo(
                  idUser: resCmts[i][Constance.user][Constance.idUser],
                  name: resCmts[i][Constance.user][Constance.name],
                  avatar: resCmts[i][Constance.user][Constance.avatar],
                  phone: resCmts[i][Constance.user][Constance.phone],
                ));
            setState(() {
              cmts.add(cmt);
            });
            print("cmt list news: ${cmt.toJson()}");
          }
        }
        News news = News(
            idNews: val.value[Constance.idNews],
            user: userInfo,
            price: val.value[Constance.price],
            area: val.value[Constance.area],
            address: val.value[Constance.address],
            lat: val.value[Constance.lat],
            lng: val.value[Constance.lng],
            desc: val.value[Constance.desc],
            time: val.value[Constance.time],
            status: val.value[Constance.status],
            images: (images != null) ? images.cast<String>().toList() : null,
            like: (likes != null) ? likes.cast<int>().toList() : null,
            cmt: cmts);
        if (news.like != null && news.like.length > 0) {
          int index = news.like.indexOf(AppConstant.user.user.idUser);
          setState(() {
            //(index >= 0) ? listStatus.add(true) : listStatus.add(false);
          });
        } else {
          setState(() {
            //listStatus.add(false);
          });
        }
        setState(() {
          listNews.add(ResNews(key: val.key, news: news));
        });
      }
      tempNews.addAll(listNews);
      if (listNews != null)
        setState(() {
          length = listNews.length;
        });
      print('list: ${listNews.length}');
    });
  }


  getItem(BuildContext context, ResNews resNews, int index) {
    print("news: ${resNews.news.toJson()}");
    List<String> imageUrls = resNews.news.images;
    bool status=false;
    if(resNews.news.like!=null &&resNews.news.like.length>0){
        int index = resNews.news.like.indexOf(AppConstant.user.user.idUser);
          if(index >= 0) status=true;
    }
    return Card(
      elevation: 4,
      shadowColor: AppConstant.colorPrimary,
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
      child: Container(
        color: Colors.white,
        padding: EdgeInsets.all(8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InkWell(
              onTap: () {
                UserInfo userInfo = UserInfo(
                    idUser: account.idUser,
                    name: account.name,
                    avatar: account.avatar,
                    phone: account.phone);
                Get.to(ProfileView(
                  account: userInfo,
                  user: resNews.news.user,
                ));
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CircleAvatar(
                        backgroundColor: AppConstant.colorPrimary,
                        radius: 16,
                        child: ClipOval(
                            child: (resNews.news.user.avatar != null &&
                                    resNews.news.user.avatar != '')
                                ? Image.network(
                                    resNews.news.user.avatar,
                                    fit: BoxFit.cover,
                                    width: 32,
                                    height: 32,
                                  )
                                : Icon(
                                    Icons.account_circle,
                                    color: Colors.white,
                                    size: 32,
                                  )),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${resNews.news.user.name}',
                            style: AppConstant.inputStyle
                                .copyWith(fontWeight: FontWeight.w500),
                          ),
                          Text(
                            '${resNews.news.time}',
                            style: AppConstant.timeStyle,
                          ),
                        ],
                      )
                    ],
                  ),
                  PopupMenuButton(
                    itemBuilder: (BuildContext context) => [
                      PopupMenuItem(
                        child: Text(
                          'Báo cáo',
                          style: AppConstant.inputStyle,
                        ),
                        value: 0,
                      ),
                      //PopupMenuItem(child: Text('Xóa bài viết',style: AppConstant.inputStyle,),value: 1),
                    ],
                    onSelected: (value) {
                      print('$value');
                      if(value==0){
                        DialogReport.dialogReport(reportController,resNews.key);
                      }
                    },
                  )
                ],
              ),
            ),
            SizedBox(
              height: 12,
            ),
            Text(
              'Diện tích: ${resNews.news.area} m2',
              style: AppConstant.infoStyle,
            ),
            SizedBox(
              height: 4,
            ),
            Text(
              'Giá: ${Utils.formatMoney(resNews.news.price)} VND/tháng',
              style: AppConstant.infoStyle,
            ),
            SizedBox(
              height: 4,
            ),
            Text(
              'Địa chỉ: ${resNews.news.address}',
              style: AppConstant.infoStyle,
            ),
            SizedBox(
              height: 4,
            ),
            (resNews.news.desc != null || resNews.news.desc == '')
                ? Text(
                    'Mô tả: ${resNews.news.desc}',
                    style: AppConstant.infoStyle,
                  )
                : Container(),
            SizedBox(
              height: 4,
            ),
            (imageUrls != null && imageUrls.length > 0)
                ? GridView.extent(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    maxCrossAxisExtent: 150,
                    mainAxisSpacing: 4.0,
                    crossAxisSpacing: 4.0,
                    children: imageUrls.map((e) {
                      return NewsItem.setImageUrl(e);
                    }).toList())
                : Container(),
            SizedBox(
              height: 6,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                (resNews.news.like == null || resNews.news.like.length == 0)
                    ? Container()
                    : InkWell(
                        onTap: () {
                          Utils.moveToScreen(
                              LikeView(
                                likes: resNews.news.like,
                              ),
                              context);
                        },
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(CupertinoIcons.suit_heart_fill,
                                size: 20, color: AppConstant.colorPrimary),
                            SizedBox(
                              width: 2,
                            ),
                            Text(
                              '${resNews.news.like.length}',
                              style: TextStyle(
                                  fontSize: 16, color: Colors.black54),
                            )
                          ],
                        ),
                      ),
                Text(
                  '${(resNews.news.cmt == null) ? 0 : resNews.news.cmt.length} bình luận',
                  style: TextStyle(fontSize: 16, color: Colors.black54),
                )
              ],
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 6),
              height: 0.5,
              color: Colors.grey,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: () {
                    setState(() {
                      status=!status;
                      updatePresenter.updateStatusLike(account.idUser, resNews);
                    });
                  },
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        CupertinoIcons.heart_fill,
                        size: 25,
                        color: (status)
                            ? AppConstant.colorPrimary
                            : Colors.grey,
                      ),
                      SizedBox(
                        width: 4,
                      ),
                      Text(
                        'Thích',
                        style: TextStyle(fontSize: 18, color: Colors.black87),
                      )
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    //Utils.moveToScreen(DetailNews(keyDB: resNews.key),context);
                    Get.to(DetailNews(keyDB: resNews.key))
                        .then((value) => getListNews());
                  },
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.speaker_notes_outlined,
                        size: 25,
                        color: AppConstant.colorPrimary,
                      ),
                      SizedBox(
                        width: 4,
                      ),
                      Text(
                        'Bình luận',
                        style: TextStyle(fontSize: 18, color: Colors.black87),
                      )
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  TextEditingController reportController=TextEditingController();

  @override
  void updateNewsSuccess(bool b, ResNews resNews) {}

  User account;

  @override
  void getInfoAccount(ResUser resUser) {
    if (resUser != null) {
      setState(() {
        account = resUser.user;
      });
    }
  }

  @override
  void updateAccountStatus(bool b, ResUser resUser) {
    // TODO: implement updateAccountStatus
  }

  @override
  void getNews(List<ResNews> data) {
    if(data!=null && data.length>0){
      setState(() {
        listNews.addAll(data);
        tempNews.addAll(data);
      });
    }
  }

  @override
  void showProgress(bool b) {
    setState(() {
      showLoading=b;
    });
  }
}
