import 'dart:ffi';
import 'dart:io';

import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_place_picker/google_maps_place_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:tttn/commons/AppConstant.dart';
import 'package:tttn/commons/Constance.dart';
import 'package:tttn/commons/Utils.dart';
import 'package:path/path.dart';
import 'package:tttn/model/News.dart';
import 'package:tttn/model/ResNews.dart';
import 'package:tttn/model/UserInfo.dart';
import 'package:tttn/view/home/HomeView.dart';

import 'DetailImage.dart';

class AddNews extends StatefulWidget {
  final int id;
  final ResNews resNews;

  const AddNews({Key key, this.id, this.resNews}) : super(key: key);

  @override
  _AddNewsState createState() => _AddNewsState();
}

class _AddNewsState extends State<AddNews> {
  List<String> images = List();
  LatLng latlng;
  String googleApiKey = AppConstant.keyGoogleMap;

  TextEditingController area = TextEditingController();
  TextEditingController price = TextEditingController();
  TextEditingController desc = TextEditingController();

  double lat, lng;
  String address;

  int id;

  final referenceDatabase = FirebaseDatabase.instance;
  bool status = true;

  @override
  void initState() {
    super.initState();
    images.add(null);
    if (widget.resNews == null) {
      status = true;
      lat = 0;
      lng = 0;
      address = '';
      id = 0;
    } else {
      status = false;
      area.text = widget.resNews.news.area.toString();
      price.text = widget.resNews.news.price.toString();
      desc.text = widget.resNews.news.desc.toString();
      lat = widget.resNews.news.lat;
      lng = widget.resNews.news.lng;
      address = widget.resNews.news.address;
      if (widget.resNews.news.images != null &&
          widget.resNews.news.images.length > 0)
        images.addAll(widget.resNews.news.images);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          (status) ? 'Thêm bài viết' : 'Cập nhật bài viết',
          style: TextStyle(fontSize: 22),
        ),
        actions: [
          InkWell(
            onTap: () {
              (status) ? createNews() : updateNews();
            },
            child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.white, width: 1),
                    borderRadius: BorderRadius.all(Radius.circular(15))),
                margin: EdgeInsets.all(8),
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                alignment: Alignment.center,
                child: Text(
                  (status) ? 'Thêm' : 'Cập nhật',
                  style: TextStyle(fontSize: 18),
                )),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 8,
              ),
              Row(
                children: [
                  Text(
                    'Diện tích (m2):',
                    style: AppConstant.inputStyle,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                      child: TextField(
                    controller: area,
                    keyboardType: TextInputType.number,
                    style: AppConstant.inputStyle,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Nhập diện tích*',
                      hintStyle: AppConstant.infoStyle,
                      isDense: true,
                      contentPadding: EdgeInsets.all(0),
                    ),
                  ))
                ],
              ),
              Utils.borderBottom(),
              Row(
                children: [
                  Text(
                    'Giá phòng:',
                    style: AppConstant.inputStyle,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                      child: TextField(
                    controller: price,
                    keyboardType: TextInputType.number,
                    style: AppConstant.inputStyle,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Nhập giá cho thuê*',
                      hintStyle: AppConstant.infoStyle,
                      isDense: true,
                      contentPadding: EdgeInsets.all(0),
                    ),
                  ))
                ],
              ),
              Utils.borderBottom(),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  InkWell(
                    onTap: () {
                      getLatLng();
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: AppConstant.colorPrimary, width: 1),
                          borderRadius: BorderRadius.all(Radius.circular(5))),
                      padding: EdgeInsets.all(4),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(
                            Icons.location_on_rounded,
                            size: 40,
                            color: AppConstant.colorPrimary,
                          ),
                          Text(
                            'Lấy vị trí',
                            style:
                                AppConstant.inputStyle.copyWith(fontSize: 16),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border(
                              bottom:
                                  BorderSide(color: Colors.grey, width: 1))),
                      child: Text(
                        (address == '' || address == null)
                            ? "Địa chỉ: chưa được chọn*"
                            : "$address",
                        style: AppConstant.infoStyle,
                        maxLines: null,
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 18,
              ),
              Text(
                'Mô tả:',
                style: AppConstant.inputStyle,
              ),
              Container(
                height: 150,
                padding: EdgeInsets.symmetric(horizontal: 4),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey, width: 1)),
                child: TextField(
                  controller: desc,
                  style: AppConstant.inputStyle,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Nhập mô tả',
                    hintStyle: AppConstant.infoStyle,
                  ),
                  maxLines: null,
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Text(
                'Hình ảnh:',
                style: AppConstant.inputStyle,
              ),
              GridView.extent(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  maxCrossAxisExtent: 100,
                  mainAxisSpacing: 4.0,
                  crossAxisSpacing: 4.0,
                  children: images.map((e) {
                    var index = images.indexOf(e);
                    if (e == null) {
                      return itemAdd();
                    }
                    return itemImage(e, index);
                  }).toList())
            ],
          ),
        ),
      ),
    );
  }

  Widget itemAdd() {
    return Container(
        height: 100,
        width: 100,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Color(0xFFBDBDBD),
        ),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(15),
            child: Stack(children: [
              InkWell(
                onTap: () async {
                  _showChoiceDialog();
                },
                child: Align(
                  alignment: Alignment.center,
                  child: Icon(
                    Icons.add_circle,
                    size: 60,
                    color: Colors.white,
                  ),
                ),
              ),
            ])));
  }

  Widget itemImage(String url, index) {
    return Stack(
      children: [
        InkWell(
          onTap: () {
            Get.to(DetailImage(), arguments: url);
          },
          child: Container(
              height: 100,
              width: 100,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: Color(0xFFBDBDBD),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(15),
                child: Image.network(
                  url,
                  fit: BoxFit.fill,
                ),
              )),
        ),
        Container(
          height: 100,
          width: 100,
          alignment: Alignment.topRight,
          child: InkWell(
            onTap: () {
              setState(() {
                images.removeAt(index);
              });
            },
            child: CircleAvatar(
              radius: 13,
              backgroundColor: Colors.red,
              child: Icon(
                Icons.clear,
                size: 25,
                color: Colors.white,
              ),
            ),
          ),
        )
      ],
    );
  }

  final picker = ImagePicker();
  bool load = false;

  Future<void> _showChoiceDialog() {
    return Get.dialog(AlertDialog(
      title: Text('Chọn nguồn ảnh!'),
      content: SingleChildScrollView(
        child: ListBody(
          children: [
            GestureDetector(
              child: Row(
                children: [
                  Icon(
                    Icons.insert_photo,
                    size: 24,
                    color: AppConstant.colorPrimary,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Text('Thư viện'),
                ],
              ),
              onTap: () {
                Get.back();
                _openGallery();
              },
            ),
            Padding(padding: EdgeInsets.all(8)),
            GestureDetector(
              child: Row(
                children: [
                  Icon(
                    Icons.camera_alt,
                    size: 24,
                    color: AppConstant.colorPrimary,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Text('Chụp ảnh'),
                ],
              ),
              onTap: () {
                Get.back();
                _openCamera();
              },
            ),
          ],
        ),
      ),
    ));
  }

  _openCamera() async {
    final pickedFile =
        await picker.getImage(source: ImageSource.camera, imageQuality: 20);
    if (pickedFile != null) {
      File _image = File(pickedFile.path);
      print('image size: ${_image.lengthSync() / (1024)}');
      await uploadImage(_image);
    }
    setState(() {
      load = false;
    });
  }

  _openGallery() async {
    var pickedFile = await picker.getImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      File _image = File(pickedFile.path);
      print('image size: ${_image.lengthSync() / (1024)}');
      await uploadImage(_image);
    }
    setState(() {
      load = false;
    });
  }

  Future uploadImage(File imageFile) async {
    setState(() {
      load = true;
    });
    var file = File(imageFile.path);
    String filename = basename(imageFile.path);
    final _storage = FirebaseStorage.instance;
    var sna = await _storage.ref().child(filename).putFile(file);
    var downloadUrl = await sna.ref.getDownloadURL();
    print("uploadedFileURL: $downloadUrl");
    setState(() {
      images.add(downloadUrl);
    });
  }

  getLatLng() async {
    latlng = await Utils.getLocation();
    if (latlng != null) {
      navigationGetLocation();
    } else {
      Utils.showToast('Không lấy được vị trí của bạn!');
    }
  }

  PickResult selectedPlace;

  navigationGetLocation() {
    Get.to(PlacePicker(
      apiKey: googleApiKey,
      initialPosition: latlng,
      useCurrentLocation: true,
      selectInitialPosition: true,
      hintText: 'Nhập địa chỉ tìm kiếm...',
      autocompleteLanguage: 'vi',
      //usePlaceDetailSearch: true,
      onPlacePicked: (result) {
        selectedPlace = result;
        Get.back();
        setState(() {
          print(
              'location: ${selectedPlace.geometry.location.lat}+${selectedPlace.geometry.location.lat}');
          print("details location: ${selectedPlace.formattedAddress}");
          lat = selectedPlace.geometry.location.lat;
          lng = selectedPlace.geometry.location.lng;
          address = selectedPlace.formattedAddress;
        });
      },
    ));
  }

  checkNull() {
    if (area.text.trim().isEmpty) {
      Utils.showToast("Vui lòng nhập thông tin diện tích!");
      return false;
    }
    if (price.text.trim().isEmpty) {
      Utils.showToast("Vui lòng nhập thông tin giá phòng!");
      return false;
    }
    if (address == '' || lng == 0 || lat == 0) {
      Utils.showToast("Vui lòng chọn địa chỉ!");
      return false;
    }
    return true;
  }

  createNews() {
    if (checkNull()) {
      Utils.getUser().then((value) {
        if (value != null) {
          UserInfo userInfo = UserInfo(
              idUser: value.user.idUser,
              name: value.user.name,
              avatar: value.user.avatar,
              phone: value.user.phone);
          try {
            int p = int.parse(price.text.trim());
            double dt = double.parse(area.text.trim());
            images.removeAt(0);
            News news = News(
                idNews: id,
                user: userInfo,
                price: p,
                area: dt,
                address: address,
                lat: lat,
                lng: lng,
                like: [],
                desc: desc.text.trim(),
                time: DateFormat('HH:mm dd/MM/yyyy').format(DateTime.now()),
                images: images,
                cmt: [],
                status: 1);
            referenceDatabase
                .reference()
                .child(Constance.news)
                .push()
                .set(news.toJson())
                .then((value) async {
              setState(() {
                load = false;
              });
              Get.off(HomeView());
              Utils.showToast("Thêm bài viết thành công!");
            }).catchError((onError) {
              setState(() {
                load = false;
              });
              Utils.showToast("Có lỗi xảy ra! Vui lòng thử lại sau!");
            });
          } catch (e) {
            Utils.showToast("Yêu cầu nhập đúng dữ liệu!");
          }
        }
      });
    }
  }

  updateNews(){
    if (checkNull()) {
      setState(() {
        load = true;
      });
          UserInfo userInfo = widget.resNews.news.user;
          try {
            int p = int.parse(price.text.trim());
            double dt = double.parse(area.text.trim());
            images.removeAt(0);
            News news = News(
                idNews: widget.resNews.news.idNews,
                user: userInfo,
                price: p,
                area: dt,
                address: address,
                lat: lat,
                lng: lng,
                like: widget.resNews.news.like,
                desc: desc.text.trim(),
                time: widget.resNews.news.time,
                images: images,
                cmt: widget.resNews.news.cmt,
                status: 1);
            FirebaseDatabase.instance
                .reference()
                .child(Constance.news)
                .child(widget.resNews.key)
                .update(news.toJson())
                .then((value) async {
              setState(() {
                load = false;
              });
              Get.off(HomeView());
              Utils.showToast("Cập nhật bài viết thành công!");
            }).catchError((onError) {
              setState(() {
                load = false;
              });
              Utils.showToast("Có lỗi xảy ra! Vui lòng thử lại sau!");
            });
          } catch (e) {
            Utils.showToast("Yêu cầu nhập đúng dữ liệu!");
          }
        }
  }
}
