import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tttn/commons/AppConstant.dart';
import 'package:tttn/controllers/ReportPresneter.dart';

class DialogReport {
  static dialogReport(TextEditingController reportController,idNews) {
    Get.defaultDialog(
      title: 'Báo cáo bài viết',
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Vui lòng nhập lý do?',style: AppConstant.textStyle,),
          TextField(
            controller: reportController,
            style: AppConstant.textStyle,
            maxLines: null,
            decoration: InputDecoration(
                hintText: "Nhập lý do...",
                hintStyle: AppConstant.textStyle.copyWith(color: Colors.grey)
            ),
          )
        ],
      ),
      titleStyle: AppConstant.labelStyle.copyWith(fontSize: 30),
      confirm: InkWell(
        onTap: () {
          ReportPresenter.createNews(reportController.text,idNews);
        },
        child: Container(
          width: 150,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: AppConstant.colorPrimary,
            borderRadius: BorderRadius.all(Radius.circular(20)),
          ),
          padding: EdgeInsets.all(8),
          child: Text(
            'Gửi',
            style: TextStyle(fontSize: 22, color: Colors.white),
          ),
        ),
      ),
      cancel: InkWell(
        onTap: () {
          Get.back();
          reportController.clear();
        },
        child: Container(
          width: 100,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            border: Border.all(color: AppConstant.colorPrimary, width: 1),
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
          ),
          padding: EdgeInsets.all(8),
          child: Text(
            'Hủy bỏ',
            style: TextStyle(fontSize: 22, color: Colors.black),
          ),
        ),
      ),
    );
  }
}