import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:tttn/commons/AppConstant.dart';
import 'package:tttn/commons/Utils.dart';
import 'package:tttn/controllers/AccountInf.dart';
import 'package:tttn/controllers/AccountPresenter.dart';
import 'package:tttn/controllers/NewsDetailInf.dart';
import 'package:tttn/controllers/NewsDetailsPresenter.dart';
import 'package:tttn/controllers/UpdateNewsInf.dart';
import 'package:tttn/controllers/UpdateNewsPresenter.dart';
import 'package:tttn/model/Cmt.dart';
import 'package:tttn/model/News.dart';
import 'package:tttn/model/ResNews.dart';
import 'package:tttn/model/ResUser.dart';
import 'package:tttn/model/User.dart';
import 'package:tttn/model/UserInfo.dart';
import 'package:tttn/view/chat/DetailChatView.dart';
import 'package:tttn/view/news/LocationView.dart';
import 'package:tttn/view/news/NewsItem.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:intl/intl.dart';

class DetailNews extends StatefulWidget {
  final String keyDB;

  const DetailNews({Key key, this.keyDB}) : super(key: key);

  @override
  _DetailNewsState createState() => _DetailNewsState();
}

class _DetailNewsState extends State<DetailNews>
    implements NewsDetailInf, AccountInf,UpdateNewsInf {
  News news;
  List<String> imageUrls = List();
  bool likeStatus = false;
  List<Cmt> cmts = List();

  NewsDetailPresenter presenter;
  AccountPresenter accountPresenter;
  UpdateNewsPresenter updateNewsPresenter;

  User account;

  bool showLoading=false;

  @override
  void initState() {
    super.initState();
    presenter = NewsDetailPresenter(this);
    presenter.getNewsDetail(widget.keyDB);
    accountPresenter = AccountPresenter(this);
    accountPresenter.loadUserInfo();
    updateNewsPresenter=UpdateNewsPresenter(this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Chi tiết bài viết'),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: (news != null)
                ? Container(
                    color: Colors.white,
                    padding: EdgeInsets.all(8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: () {},
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  CircleAvatar(
                                    backgroundColor: AppConstant.colorPrimary,
                                    radius: 16,
                                    child: ClipOval(
                                        child: (news.user.avatar != null &&
                                                news.user.avatar != '')
                                            ? Image.network(
                                                news.user.avatar,
                                                fit: BoxFit.cover,
                                                width: 32,
                                                height: 32,
                                              )
                                            : Icon(
                                                Icons.account_circle,
                                                color: Colors.white,
                                                size: 32,
                                              )),
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        '${news.user.name}',
                                        style: AppConstant.inputStyle
                                            .copyWith(fontWeight: FontWeight.w500),
                                      ),
                                      Text(
                                        '${news.time}',
                                        style: AppConstant.timeStyle,
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  InkWell(
                                    onTap: () async {
                                      await launch("tel:${news.user.phone}");
                                    },
                                    child: Container(
                                      height: 40,
                                      width: 40,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.all(Radius.circular(30)),
                                          color: AppConstant.colorPrimary),
                                      child: Icon(
                                        Icons.phone,
                                        color: Colors.white,
                                        size: 26,
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 8,),
                                  InkWell(
                                    onTap: () {
                                      UserInfo userInfo = UserInfo(
                                          idUser: account.idUser,
                                          name: account.name,
                                          avatar: account.avatar,
                                          phone: account.phone);
                                      Get.to(DetailChatView(user1: userInfo,user2: news.user,));
                                    },
                                    child: Container(
                                      height: 40,
                                      width: 40,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                          BorderRadius.all(Radius.circular(30)),
                                          color: AppConstant.colorPrimary),
                                      child: Icon(
                                        Icons.message,
                                        color: Colors.white,
                                        size: 26,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        Text(
                          'Diện tích: ${news.area} m2',
                          style: AppConstant.infoStyle,
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        Text(
                          'Giá: ${Utils.formatMoney(news.price)} VND/tháng',
                          style: AppConstant.infoStyle,
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        Text(
                          'Địa chỉ: ${news.address}',
                          style: AppConstant.infoStyle,
                        ),
                        SizedBox(
                          height: 4,
                        ),
                        (news.desc != null || news.desc == '')
                            ? Text(
                                'Mô tả: ${news.desc}',
                                style: AppConstant.infoStyle,
                              )
                            : Container(),
                        SizedBox(
                          height: 4,
                        ),
                        (imageUrls != null && imageUrls.length > 0)
                            ? GridView.extent(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                maxCrossAxisExtent: 150,
                                mainAxisSpacing: 4.0,
                                crossAxisSpacing: 4.0,
                                children: imageUrls.map((e) {
                                  return NewsItem.setImageUrl(e);
                                }).toList())
                            : Container(),
                        SizedBox(
                          height: 6,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            (news.like == null || news.like.length == 0)
                                ? Container()
                                : Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Icon(CupertinoIcons.heart_fill,
                                          size: 20,
                                          color: AppConstant.colorPrimary),
                                      SizedBox(
                                        width: 2,
                                      ),
                                      Text(
                                        '${news.like.length}',
                                        style: TextStyle(
                                            fontSize: 16, color: Colors.black54),
                                      )
                                    ],
                                  ),
                            Text(
                              '${(news.cmt == null) ? 0 : news.cmt.length} bình luận',
                              style: TextStyle(fontSize: 16, color: Colors.black54),
                            )
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 6),
                          height: 0.5,
                          color: Colors.grey,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    likeStatus = !likeStatus;
                                    updateNewsPresenter.updateStatusLike(account.idUser, ResNews(key: key,news: news));
                                  });
                                },
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Icon(
                                      CupertinoIcons.heart_fill,
                                      size: 25,
                                      color: (likeStatus)
                                          ? AppConstant.colorPrimary
                                          : Colors.grey,
                                    ),
                                    SizedBox(
                                      width: 4,
                                    ),
                                    Text(
                                      'Thích',
                                      style: TextStyle(
                                          fontSize: 18, color: Colors.black87),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                Utils.moveToScreen(
                                    Locationview(
                                      lng: news.lng,
                                      lat: news.lat,
                                      address: news.address,
                                    ),
                                    context);
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: AppConstant.colorPrimary,
                                        width: 1.5),
                                    borderRadius: BorderRadius.circular(20)),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 6, vertical: 3),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Icon(Icons.location_on,
                                        size: 25, color: AppConstant.colorPrimary),
                                    SizedBox(
                                      width: 4,
                                    ),
                                    Text(
                                      'Xem vị trí',
                                      style: style1,
                                    )
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 6),
                          height: 0.5,
                          color: Colors.grey,
                        ),
                        actionCmt(),
                        listCmt(news.cmt)
                      ],
                    ),
                  )
                : Container(
                    color: Colors.white,
                  ),
          ),
          (showLoading)?Utils.CircularProgress(context):Container()
        ],
      ),
    ); //:Container();
  }

  TextStyle style1 = TextStyle(fontSize: 18, color: Colors.black87);

  TextEditingController content = TextEditingController();

  actionCmt() {
    return (account != null)
        ? Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
          child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CircleAvatar(
                  backgroundColor: AppConstant.colorPrimary,
                  radius: 14,
                  child: ClipOval(
                      child: (account.avatar != null && account.avatar != '')
                          ? Image.network(
                              account.avatar,
                              fit: BoxFit.cover,
                              width: 28,
                              height: 28,
                            )
                          : Icon(
                              Icons.account_circle,
                              color: Colors.white,
                              size: 28,
                            )),
                ),
                SizedBox(
                  width: 8,
                ),
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${account.name}',
                      style: style1,
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                        ),
                        padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: TextField(
                                controller: content,
                                style: style1.copyWith(color: Colors.black54),
                                maxLines: null,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: 'Nhập nội dung bình luận...',
                                  hintStyle: AppConstant.infoStyle,
                                  isDense: true,
                                  contentPadding: EdgeInsets.all(0),
                                ),
                              ),
                            ),
                            IconButton(
                                icon: Icon(
                                  Icons.send_rounded,
                                  color: AppConstant.colorPrimary,
                                  size: 30,
                                ),
                                onPressed: () {
                                  updateNews();
                                })
                          ],
                        ))
                  ],
                ))
              ],
            ),
        )
        : Container();
  }

  updateNews() {
    if (content.text.trim().isNotEmpty) {
      Cmt cmt = Cmt(
          idCmt: (news.cmt == null) ? 0 : news.cmt.length,
          content: content.text.trim(),
          user: UserInfo(
              idUser: account.idUser,
              name: account.name,
              phone: account.phone,
              avatar: account.avatar),
          time: DateFormat('HH:mm dd/MM/yyyy').format(DateTime.now()));
      print("news update: ${cmt.toJson()}");
      setState(() {
        cmts.add(cmt);
      });
      ResNews resNews = ResNews(
          key: key,
          news: News(
              idNews: news.idNews,
              user: news.user,
              price: news.price,
              area: news.area,
              address: news.address,
              lat: news.lat,
              lng: news.lng,
              desc: news.desc,
              time: news.time,
              status: news.status,
              images: imageUrls,
              like: news.like,
              cmt: cmts));
      updateNewsPresenter.updateNews(resNews);
    } else {
      Utils.showToast("Bạn chưa nhập nội dung bình luận!");
    }
  }

  listCmt(List<Cmt> cmts) {
    return (cmts != null && cmts.length > 0)
        ? ListView.builder(
            itemCount: cmts.length,
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemBuilder: (BuildContext context, int index) {
              return getItemCmt(cmts[index]);
            })
        : Container();
  }

  getItemCmt(Cmt cmt) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CircleAvatar(
            backgroundColor: AppConstant.colorPrimary,
            radius: 14,
            child: ClipOval(
                child: (cmt.user.avatar != null && cmt.user.avatar != '')
                    ? Image.network(
                        cmt.user.avatar,
                        fit: BoxFit.cover,
                        width: 28,
                        height: 28,
                      )
                    : Icon(
                        Icons.account_circle,
                        color: Colors.white,
                        size: 28,
                      )),
          ),
          SizedBox(width: 8,),
          Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '${cmt.user.name}',
                style: style1,
              ),
              Text(
                '${cmt.time}',
                style: style1.copyWith(color: Colors.black54,fontSize: 12),
              ),
              Text(
                '${cmt.content}',maxLines: null,
                style: style1.copyWith(color: Colors.black54),
              ),
            ],
          ))
        ],
      ),
    );
  }

  String key;

  @override
  void getNewsDetailSuccess(ResNews resNews) {
    if (resNews != null) {
      setState(() {
        key = resNews.key;
        news = resNews.news;
        imageUrls = resNews.news.images;
        if (resNews.news.like != null) {
          int index = resNews.news.like.indexOf(AppConstant.user.user.idUser);
          (index >= 0) ? likeStatus = true : likeStatus = false;
        }
        if (resNews.news.cmt != null && resNews.news.cmt.length > 0)
          cmts.addAll(resNews.news.cmt);
      });
    } else {

    }
  }

  @override
  void getInfoAccount(ResUser resUser) {
    if (resUser != null) {
      setState(() {
        account = resUser.user;
      });
    }
  }

  @override
  void updateAccountStatus(bool b, ResUser resUser) {}

  @override
  void updateNewsSuccess(bool b, ResNews resNews) {
    if(b){
      setState(() {
        news=resNews.news;
        key=resNews.key;
        content.clear();
      });
    }
  }

  @override
  void showProgress(bool b) {
    setState(() {
      showLoading=b;
    });
  }
}
