import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tttn/commons/AppConstant.dart';
import 'package:tttn/commons/Utils.dart';
import 'package:tttn/model/News.dart';
import 'package:tttn/model/ResNews.dart';
import 'package:tttn/view/news/DetailImage.dart';
import 'package:tttn/view/news/DetailNews.dart';

class NewsItem{
  static getItem(BuildContext context,ResNews resNews){
    print("news: ${resNews.news.toJson()}");
    List<String> imageUrls=resNews.news.images;
    bool likeStatus;
    if(resNews.news.like != null){
      int index=resNews.news.like.indexOf(AppConstant.user.user.idUser);
        (index>=0)?likeStatus=true:likeStatus=false;
    }
    return Card(
      elevation: 4,
      shadowColor: AppConstant.colorPrimary,
      margin: EdgeInsets.symmetric(vertical: 8),
      child: Container(
        color: Colors.white,
        padding: EdgeInsets.all(8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InkWell(
              onTap: (){

              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CircleAvatar(
                        backgroundColor: AppConstant.colorPrimary,
                        radius: 16,
                        child: (resNews.news.user.avatar!=null && resNews.news.user.avatar!='')?Image.network(resNews.news.user.avatar):Icon(Icons.account_circle,color: Colors.white,size: 32,),
                      ),
                      SizedBox(width: 8,),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('${resNews.news.user.name}',style: AppConstant.inputStyle.copyWith(fontWeight: FontWeight.w500),),
                          Text('${resNews.news.time}',style: AppConstant.timeStyle,),
                        ],
                      )
                    ],
                  ),
                  PopupMenuButton(itemBuilder: (BuildContext context)=>[
                    PopupMenuItem(child: Text('Báo cáo',style: AppConstant.inputStyle,),value: 0,),
                    PopupMenuItem(child: Text('Xóa bài viết',style: AppConstant.inputStyle,),value: 1),
                  ],
                  onSelected: (value){
                    print('$value');
                  },)
                ],
              ),
            ),
            SizedBox(height: 12,),
            Text('Diện tích: ${resNews.news.area} m2',style: AppConstant.infoStyle,),
            SizedBox(height: 4,),
            Text('Giá: ${Utils.formatMoney(resNews.news.price)} VND/tháng',style: AppConstant.infoStyle,),
            SizedBox(height: 4,),
            Text('Địa chỉ: ${resNews.news.address}',style: AppConstant.infoStyle,),
            SizedBox(height: 4,),
            (resNews.news.desc!=null ||resNews.news.desc=='')?Text('Mô tả: ${resNews.news.desc}',style: AppConstant.infoStyle,):Container(),
            SizedBox(height: 4,),
            (imageUrls!=null && imageUrls.length>0)?GridView.extent(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                maxCrossAxisExtent: 150,
                mainAxisSpacing: 4.0,
                crossAxisSpacing: 4.0,
                children: imageUrls.map((e) {
                  return setImageUrl(e);
                }).toList()
                    ):Container(),
            SizedBox(height: 6,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                (resNews.news.like==null||resNews.news.like.length==0)?Container():Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(Icons.whatshot,size: 20,color:AppConstant.colorPrimary),
                    SizedBox(width: 2,),
                    Text('${resNews.news.like.length}',style: TextStyle(fontSize: 16,color: Colors.black54),)
                  ],
                ),
                Text('${(resNews.news.cmt==null)?0:resNews.news.cmt.length} bình luận',style: TextStyle(fontSize: 16,color: Colors.black54),)
              ],
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 6),
              height: 0.5,
              color: Colors.grey,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: (){

                  },
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(Icons.whatshot,size: 25,color: (likeStatus)? AppConstant.colorPrimary:Colors.grey,),
                      SizedBox(width: 4,),
                      Text('Thích',style: TextStyle(fontSize: 18,color: Colors.black87),)
                    ],
                  ),
                ),
                InkWell(
                  onTap: (){
                   Utils.moveToScreen(DetailNews(keyDB: resNews.key),context);
                  },
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(Icons.speaker_notes_outlined,size: 25,color: AppConstant.colorPrimary,),
                      SizedBox(width: 4,),
                      Text('Bình luận',style: TextStyle(fontSize: 18,color: Colors.black87),)
                    ],
                  ),
                ),
                // InkWell(
                //   onTap: (){
                //       Get.to(DetailNews(keyDB: key));
                //     // print("news push: ${news.toJson()}");
                //     //   Navigator.push(
                //     //     context,
                //     //     MaterialPageRoute(builder: (context) => DetailNews(keyDB: key,news: news,)),
                //     //   );
                //   },
                //   child: Container(
                //     decoration: BoxDecoration(
                //       border: Border.all(color: AppConstant.colorPrimary,width: 1.5),
                //       borderRadius: BorderRadius.circular(20)
                //     ),
                //       padding: EdgeInsets.symmetric(horizontal: 6,vertical: 3),
                //       child: Text('Xem chi tiết',style: TextStyle(fontSize: 18,color: Colors.black87),)),
                // )
              ],
            )
          ],
        ),
      ),
    );
  }

  static Widget setImageUrl(String url) {
    return Container(
      height: 150,
      width: 150,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Color(0xFFBDBDBD),
      ),
      child: ClipRRect(
          borderRadius: BorderRadius.circular(15),
          child: GestureDetector(
            onTap: () {
                Get.to(DetailImage(),arguments: url);
            },
            child: Image.network(
              url,
              width: 150,
              height: 150,
              fit: BoxFit.cover,
              errorBuilder: (BuildContext context, Object exception,
                  StackTrace stackTrace) {
                return Center(
                  child: Icon(
                    Icons.error,
                    color: Colors.white,
                    size: 60,
                  ),
                );
              },
              loadingBuilder: (BuildContext ctx, Widget child,
                  ImageChunkEvent loadingProgress) {
                if (loadingProgress == null) {
                  return child;
                } else {
                  return Center(
                    child: CircularProgressIndicator(
                      strokeWidth: 3,
                      valueColor:
                      AlwaysStoppedAnimation<Color>(AppConstant.colorPrimary),
                    ),
                  );
                }
              },
            ),
          )),
    );
  }
}