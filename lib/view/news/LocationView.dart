import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:tttn/commons/GoogleMapsServices.dart';
import 'package:tttn/commons/Utils.dart';

class Locationview extends StatefulWidget {
  final double lat;
  final double lng;
  final String address;

  const Locationview({Key key, this.lat, this.lng, this.address}) : super(key: key);

  @override
  _LocationviewState createState() => _LocationviewState();
}

class _LocationviewState extends State<Locationview> {

  CameraPosition _currentCameraPosition;
  Completer<GoogleMapController> _controller = Completer();
  CameraPosition _initPos;
  final Set<Marker> _markers = {};
  BitmapDescriptor _salePointIcon;
  LatLng _currentLatLng;
  bool _locationServiceEnabled;
  PermissionStatus _locationPermissionGranted;
  final GoogleMapsServices _googleMapsServices = GoogleMapsServices();
  Set<Polyline> _polyLines = {};

  Set<Polyline> get polyLines => _polyLines;
  final location = Location();
  bool _findingDirection = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // BitmapDescriptor.fromAssetImage(
    //   ImageConfiguration(devicePixelRatio: 2.5),
    //   'assets/icon/ic.png',
    // ).then(
    //       (onValue) {
    //     _salePointIcon = onValue;
    //   },
    // );

    _currentCameraPosition = CameraPosition(
      target: LatLng(widget.lat, widget.lng),
      zoom: 19,
    );

    _initPos = CameraPosition(
      target: LatLng(widget.lat, widget.lng),
      zoom: 19,
    );
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Xem vị trí'),
      ),
      body: Stack(
        children: [
          GoogleMap(
            zoomControlsEnabled: false,
            markers: _markers,
            mapType: MapType.normal,
            polylines: polyLines,
            initialCameraPosition: _initPos,
            myLocationEnabled: true,
            onMapCreated: (GoogleMapController controller) {
              _controller.complete(controller);
              genMarkers();
            },
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0,
        onPressed: _findingDirection ? () {} : drawDirection,
        child: _findingDirection
            ? Container(
          width: 20,
          height: 20,
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
            backgroundColor: Theme.of(context).primaryColor,
            strokeWidth: 2,
          ),
        )
            :  Icon(Icons.directions,size: 45,),
      ),
    );
  }

  void drawDirection() async {
    bool checkNetwork = await Utils.hasConnection();
    if (checkNetwork) {
      setState(() {
        _findingDirection = true;
      });
      await getLocation();
      var enable = await Utils.checkGps();
      if (enable) {
        LatLng destination =
        LatLng(widget.lat, widget.lng);
        String route = await _googleMapsServices.getRouteCoordinates(
            origin: _currentLatLng, destination: destination);
        createRoute(route);
        setState(() {
          _findingDirection = false;
        });
      } else {
        Utils.showToast(
            "Không lấy được vị trí của bạn! \n Vui lòng bật định vị GPS để thực hiện hành động!");
        setState(() {
          _findingDirection = false;
        });
      }
    } else {
      Utils.showToast("Vui lòng kiểm tra kết nối internet!");
    }
    // _addMarker(destination,"KTHM Collage");
  }
  void createRoute(String encondedPoly) {
    setState(() {
      _polyLines = {Polyline(
          polylineId: PolylineId(_currentLatLng.toString(),), width: 6,
          points: _convertToLatLng(_decodePoly(encondedPoly),),
          color: Theme.of(context).primaryColor,
        ),
      };
      _currentCameraPosition = CameraPosition(
        target: _currentLatLng, zoom: 13,);
      _changeMapPosition();
    });
  }

  Future<void> _changeMapPosition() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_currentCameraPosition));
  }

  List<LatLng> _convertToLatLng(List points) {
    List<LatLng> result = <LatLng>[];
    for (int i = 0; i < points.length; i++) {
      if (i % 2 != 0) {
        result.add(LatLng(points[i - 1], points[i]));
      }
    }
    return result;
  }

  List _decodePoly(String poly) {
    var list = poly.codeUnits;
    var lineList = new List();
    int index = 0;
    int len = poly.length;
    int c = 0;
    do {
      var shift = 0;
      int result = 0;
      do {
        c = list[index] - 63;
        result |= (c & 0x1F) << (shift * 5);
        index++;
        shift++;
      } while (c >= 32);
      if (result & 1 == 1) {
        result = ~result;
      }
      var result1 = (result >> 1) * 0.00001;
      lineList.add(result1);
    } while (index < len);
    for (var i = 2; i < lineList.length; i++) {
      lineList[i] += lineList[i - 2];
    }
    return lineList;
  }

  Future<void> getLocation() async {
    _locationServiceEnabled = await location.serviceEnabled();
    if (!_locationServiceEnabled) {
      _locationServiceEnabled = await location.requestService();
      if (!_locationServiceEnabled) {
        return null;
      }
    }
    _locationPermissionGranted = await location.hasPermission();
    if (_locationPermissionGranted == PermissionStatus.denied) {
      _locationPermissionGranted = await location.requestPermission();
      if (_locationPermissionGranted != PermissionStatus.granted) {
        return null;
      }
    }

    final curLocation = await location.getLocation();
    _currentLatLng = LatLng(curLocation.latitude, curLocation.longitude);
    print('LOCATION: $_currentLatLng');
  }
  void genMarkers() {
    setState(() {
      //_markers = Set<Marker>();
        _markers.add(
          Marker(
            markerId: MarkerId("id"),
            position: LatLng(widget.lat, widget.lng),
            icon: BitmapDescriptor.defaultMarker,
            consumeTapEvents: true,
            // onTap: () {
            //   _changeCurrentPosition();
            // },
          ),
        );
    });
  }
}
