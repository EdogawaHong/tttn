import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tttn/commons/AppConstant.dart';
import 'package:tttn/commons/Utils.dart';
import 'package:tttn/controllers/LikeInf.dart';
import 'package:tttn/controllers/LikePresenter.dart';
import 'package:tttn/model/ResUser.dart';
import 'package:tttn/model/User.dart';

class LikeView extends StatefulWidget {
  final List<int> likes;

  const LikeView({Key key, this.likes}) : super(key: key);

  @override
  _LikeScreenState createState() => _LikeScreenState();
}

class _LikeScreenState extends State<LikeView> implements LikeInf{
  List<ResUser> users = List();

  LikePresenter _presenter;

  bool showLoading=false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _presenter=LikePresenter(this);
    _presenter.getListUser(widget.likes);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Người bày tỏ cảm xúc",
          style: TextStyle(fontSize: 22),
        ),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      Icon(
                        CupertinoIcons.heart_fill,
                        color: AppConstant.colorPrimary,
                      ),
                      Text(
                        "${widget.likes.length}",
                        style: TextStyle(fontSize: 18, color: Colors.black54),
                      )
                    ],
                  ),
                  Container(height: 1,color: Colors.grey,),
                  (users != null && users.length > 0)
                      ? ListView.builder(
                    shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: users.length,
                          itemBuilder: (context, index) {
                            return itemUser(context, users[index].user);
                          })
                      : Container()
                ],
              ),
            ),
          ),
          (showLoading)?Utils.CircularProgress(context):Container()
        ],
      ),
    );
  }

  itemUser(context, User user) {
    return Padding(
      padding: const EdgeInsets.only(left: 8,top: 8),
      child: Row(
        children: [
          CircleAvatar(
            backgroundColor: AppConstant.colorPrimary,
            radius: 16,
            child: ClipOval(
                child: (user.avatar != null && user.avatar != '')
                    ? Image.network(
                        user.avatar,
                        fit: BoxFit.cover,
                        width: 32,
                        height: 32,
                      )
                    : Icon(
                        Icons.account_circle,
                        color: Colors.white,
                        size: 32,
                      )),
          ),
          SizedBox(width: 5,),
          Expanded(child: Text("${user.name}",style: TextStyle(fontSize: 18, color: Colors.black54),))
        ],
      ),
    );
  }

  @override
  void getListLike(List<ResUser> data) {
    // TODO: implement getListLike
    setState(() {
      users.clear();
      if(data!=null){
        users.addAll(data);
      }
    });
  }

  @override
  void showProgress(bool b) {
    setState(() {
      showLoading=b;
    });
  }
}
