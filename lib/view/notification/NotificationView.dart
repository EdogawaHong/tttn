import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tttn/commons/AppConstant.dart';

class NotificationView extends StatefulWidget {
  @override
  _NotificationViewState createState() => _NotificationViewState();
}

class _NotificationViewState extends State<NotificationView> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Column(
      children: [
        Container(
          color: AppConstant.colorPrimary,
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.symmetric(horizontal: 8,vertical: 16),
          child: Text(
            "Thông báo",
            style: TextStyle(fontSize: 22),
          ),
        ),
        Expanded(
          child: ListView.builder(
            itemCount: 5,
              shrinkWrap: true,
              itemBuilder: (BuildContext context,int index){
                return getItemNoti(context, index);
          }),
        ),
      ],
    ));
  }

  getItemNoti(BuildContext context, int index){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5),
      child: Card(
        elevation: 2,
        shadowColor: AppConstant.colorPrimary,
        margin: EdgeInsets.symmetric(vertical: 8),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8,horizontal: 4),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Icon(Icons.notifications,size: 35,color: AppConstant.colorPrimary,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('This is title',style: AppConstant.textStyle,),
                  Text('This is content',style: AppConstant.infoStyle,),
                  Text('15:23 11/03/2021',style: AppConstant.timeStyle,),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
