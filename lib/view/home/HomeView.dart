import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tttn/commons/AppConstant.dart';
import 'package:tttn/commons/Constance.dart';
import 'package:tttn/commons/SharedPref.dart';
import 'package:tttn/commons/Utils.dart';
import 'package:tttn/model/User.dart';
import 'package:tttn/view/account/AccountView.dart';
import 'package:tttn/view/chat/ChatView.dart';
import 'package:tttn/view/news/NewsView.dart';
import 'package:tttn/view/notification/NotificationView.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  int selectedIndex = 0;
  PageController _pageController;
  String title;

  @override
  void initState() {
    super.initState();
    title="Trang chủ";
    _pageController = PageController(
      initialPage: selectedIndex,
      keepPage: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text("$title"),
      //   backgroundColor: AppConstant.colorPrimary,
      // ),
      body: Container(
        padding: EdgeInsets.only(top: 0),
        color: AppConstant.colorPrimary,
        child: Container(color: Colors.white, child: pageView()),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
      ),
      bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }

  Widget pageView() {
    return PageView(
      physics: NeverScrollableScrollPhysics(),
      controller: _pageController,
      pageSnapping: false,
      children: [NewsView(), ChatView(), NotificationView(), AccountView()],
    );
  }

  Widget _buildBottomNavigationBar() {
    return BottomNavigationBar(
      onTap: (index) {
        setState(() {
          selectedIndex = index;
          switch(index){
            case 0: title="Trang chủ";break;
            case 1: title="Nhắn tin";break;
            case 2: title="Thông báo";break;
            case 3: title="Tài khoản";break;
            default: title="Trang chủ";break;
          }
        });
        _pageController.animateToPage(selectedIndex,
            duration: Duration(milliseconds: 300), curve: Curves.easeInCubic);
      },
      items: [
        BottomNavigationBarItem(
          icon: Icon(
            CupertinoIcons.home,
            size: 30,
          ),
          title: Text('Trang chủ'),
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.message,
            size: 30,
          ),
          title: Text('Nhắn tin'),
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.notifications,
            size: 30,
          ),
          title: Text('Thông báo'),
        ),
        BottomNavigationBarItem(
          icon: Icon(
            CupertinoIcons.person,
            size: 30,
          ),
          title: Text('Tài khoản'),
        ),
      ],
      currentIndex: selectedIndex,
      backgroundColor: Colors.white,
      selectedItemColor: AppConstant.colorPrimary,
      unselectedItemColor: Colors.black87,
      selectedLabelStyle: TextStyle(
          fontSize: 17,
          color: AppConstant.colorPrimary,
          fontWeight: FontWeight.w500),
      unselectedLabelStyle: TextStyle(
          fontSize: 14, color: Colors.black87, fontWeight: FontWeight.w500),
      showUnselectedLabels: true,
      type: BottomNavigationBarType.fixed,
    );
  }
}
