import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:tttn/commons/AppConstant.dart';
import 'package:tttn/commons/Utils.dart';
import 'package:tttn/controllers/ChatInf.dart';
import 'package:tttn/controllers/ChatPresenter.dart';
import 'package:tttn/controllers/UserInf.dart';
import 'package:tttn/controllers/UserPresenter.dart';
import 'package:tttn/model/Chat.dart';
import 'package:tttn/model/ResChat.dart';
import 'package:tttn/model/ResUser.dart';
import 'package:tttn/model/User.dart';
import 'package:tttn/model/UserInfo.dart';
import 'package:tttn/view/chat/DetailChatView.dart';

class ChatView extends StatefulWidget {
  @override
  _ChatViewState createState() => _ChatViewState();
}

class _ChatViewState extends State<ChatView> implements ChatInf,UserInf{
  bool showLoading = false;

  List<ResChat> listChat=List();
  List<ResUser> listUsers=List();
  ChatPresenter _chatPresenter;
  UserPresenter _userPresenter;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _userPresenter=UserPresenter(this);
    _userPresenter.getListUsers();
    _chatPresenter=ChatPresenter(this);
     //_chatPresenter.getListChat();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
          Container(
            color: AppConstant.colorPrimary,
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.symmetric(horizontal: 8,vertical: 16),
            child: Text(
              "Nhắn tin",
              style: TextStyle(fontSize: 22),
            ),
          ),
          Expanded(
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: (listChat!=null && listChat.length>0)?ListView.builder(
                      itemCount: listChat.length,
                      itemBuilder: (context, index) {
                        return itemChat(context, listChat[index].chat);
                      }):Container(),
                ),
                (showLoading) ? Utils.CircularProgress(context) : Container()
              ],
            ),
          ),
        ],
      ),
    );
  }

  itemChat(context, Chat item) {
    print("item: ${item.toJson()}");
    int idReceiver=item.idSender;
    if(item.idSender==AppConstant.idUser){
      idReceiver=item.idReceiver;
    }
    User user;
    if(listUsers!=null && listUsers.length>0){
      listUsers.forEach((e) {
        if(e.user.idUser==idReceiver){
          user=e.user;
        }
      });
    }
    String lastMessage="Thêm liên hệ";
    String time="";
    if(item.items!=null && item.items.length>0){
      DateTime date=DateFormat("HH:mm dd/MM/yyyy").parse(item.items[item.items.length-1].time);
      DateTime now=DateTime.now();
      if(date.year==now.year && date.month==now.month && date.day==now.day){
        time=DateFormat("HH:mm").format(date);
      }else{
        time=DateFormat("dd/MM").format(date);
      }
      lastMessage=item.items[item.items.length-1].content;
    }
    return InkWell(
      onTap: (){
        if(user!=null) {
          UserInfo myAccount = UserInfo(
              idUser: AppConstant.user.user.idUser,
              name: AppConstant.user.user.name,
              avatar: AppConstant.user.user.avatar,
              phone: AppConstant.user.user.phone);
          UserInfo receiver = UserInfo(
              idUser: user.idUser,
              name: user.name,
              avatar: user.avatar,
              phone: user.phone);
          Utils.moveToScreenCallback(DetailChatView(
            user1: myAccount,
            user2: receiver,
          ), context,()=>_chatPresenter.getListChat());
        }else {
          Utils.showToast("Có lỗi xảy ra. Vui lòng thử lại sau!");
        }
      },
      child: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Row(
          children: [
            CircleAvatar(
              backgroundColor: AppConstant.colorPrimary,
              radius: 20,
              child:ClipOval(
                  child: (user!=null && user.avatar != null && user.avatar != '')
                      ? Image.network(
                    user.avatar,
                    fit: BoxFit.cover,
                    width: 40,
                    height: 40,
                  )
                      : Icon(
                    Icons.account_circle,
                    color: Colors.white,
                    size: 40,
                  )),
            ),
            SizedBox(
              width: 5,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    (user!=null && user.name!=null &&user.name.isNotEmpty)?'${user.name}':"Người dùng ứng dụng",
                    style: TextStyle(fontSize: 18, color: Colors.black87,fontWeight: FontWeight.w700),
                  ),
                  Text(
                    "$lastMessage $time",
                    style: TextStyle(fontSize: 16, color: Colors.black87,fontStyle: FontStyle.italic),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  void addChat(bool b) {
    // TODO: implement addChat
  }

  @override
  void getChat(ResChat resChat) {
    // TODO: implement getChat
  }

  @override
  void getListChat(List<ResChat> data) {
    if(data!=null && data.length>0){
      listChat.clear();
      data.forEach((e) {
        if(e.chat.idReceiver==AppConstant.idUser||e.chat.idSender==AppConstant.idUser){
          setState(() {
            listChat.add(e);
          });
        }
      });
    }
    print("listChat lenght: ${listChat.length}");
    print("AppConstant.idUser: ${AppConstant.idUser}");
  }

  @override
  void showProgress(bool b) {
    setState(() {
      showLoading=b;
    });
  }

  @override
  void getListUser(List<ResUser> listUsers) {
    setState(() {
      this.listUsers=listUsers;
    });
    _chatPresenter.getListChat();
  }

  @override
  void loginSuccess(ResUser resUser) {
    // TODO: implement loginSuccess
  }

  @override
  void registerSuccess(bool b) {
    // TODO: implement registerSuccess
  }
}
