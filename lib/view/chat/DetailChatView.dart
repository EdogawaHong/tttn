import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:tttn/commons/AppConstant.dart';
import 'package:tttn/controllers/ChatInf.dart';
import 'package:tttn/controllers/ChatPresenter.dart';
import 'package:tttn/model/Chat.dart';
import 'package:tttn/model/ChatItem.dart';
import 'package:tttn/model/ResChat.dart';
import 'package:tttn/model/ResUser.dart';
import 'package:tttn/model/UserInfo.dart';

class DetailChatView extends StatefulWidget {
  final UserInfo user1;//me
  final UserInfo user2;

  const DetailChatView({Key key, this.user1, this.user2}) : super(key: key);

  @override
  _DetailChatViewState createState() => _DetailChatViewState();
}

class _DetailChatViewState extends State<DetailChatView> implements ChatInf{
  UserInfo user1;
  UserInfo user2;
  TextEditingController content = TextEditingController();
  
  ChatPresenter _chatPresenter;
  
  List<ResChat> listResChat=List();

  int idReceiver;
  int idSender;

  ResChat chatInfo;

  @override
  void initState() {
    super.initState();
    user1 = widget.user1;
    user2 = widget.user2;
    idSender=widget.user1.idUser;
    idReceiver=widget.user2.idUser;
    _chatPresenter=ChatPresenter(this);
    _chatPresenter.getListChat();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 16,
              child: ClipOval(
                  child: (user2.avatar != null && user2.avatar != '')
                      ? Image.network(
                          user2.avatar,
                          fit: BoxFit.cover,
                          width: 32,
                          height: 32,
                        )
                      : Icon(
                          Icons.account_circle,
                          color: Colors.white,
                          size: 32,
                        )),
            ),
            SizedBox(
              width: 8,
            ),
            Text(
              '${user2.name}',
              style: TextStyle(fontSize: 22),
            ),
          ],
        ),
      ),
      body: Container(
        alignment: Alignment.bottomCenter,
        width: Get.width,
        height: Get.height,
        margin: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
                child: Container(
                  alignment: Alignment.bottomCenter,
                  child: (chatInfo!=null && chatInfo.chat.items!=null)?ListView.builder(
                      shrinkWrap: true,
                      itemCount: chatInfo.chat.items.length,
                      itemBuilder: (context, index) => itemMessage(context,chatInfo.chat.items[index])):Container(),
                )),
            Container(
                alignment: Alignment.bottomCenter,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey, width: 1),
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                ),
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: TextField(
                        controller: content,
                        style: TextStyle(fontSize: 18, color: Colors.black87),
                        maxLines: null,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Nhập nội dung ...',
                          hintStyle: AppConstant.infoStyle,
                          isDense: true,
                          contentPadding: EdgeInsets.only(top: 6),
                        ),
                      ),
                    ),
                    IconButton(
                        icon: Icon(
                          Icons.send_rounded,
                          color: AppConstant.colorPrimary,
                          size: 30,
                        ),
                        onPressed: () {
                          if(content.text.trim().isNotEmpty && content.text!=null){
                            sendMessage();
                          }
                        })
                  ],
                ))
          ],
        ),
      ),
    );
  }

  itemMessage(BuildContext context,ChatItem item){
    return Container(
      padding: EdgeInsets.only(left: 8,right: 8,top: 0,bottom: 8),
      child: Align(
        alignment: (item.idUser==idReceiver?Alignment.topLeft:Alignment.topRight),
        child: Container(
          constraints: BoxConstraints(maxWidth: Get.width*2/3),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: (item.idUser==idReceiver?Colors.grey.shade200:AppConstant.colorPrimary),
          ),
          padding: EdgeInsets.all(8),
          child: Text("${item.content}", style: TextStyle(color: Colors.black)),
        ),
      ),
    );
  }

  sendMessage() {
    ChatItem item=ChatItem(
        content: content.text.trim(),
        idUser: idSender,
        time: DateFormat('HH:mm dd/MM/yyyy').format(DateTime.now())
    );
    if(chatInfo!=null){
      ResChat req=chatInfo;
      req.chat.items.add(item);
      _chatPresenter.sendMess(req);
    }else{
      _chatPresenter.addChat(idReceiver, idSender, item);
    }
  }

  @override
  void addChat(bool b) {
    if(b){
      //_chatPresenter.checkMess(idReceiver, idSender, listResChat);
      content.text="";
      _chatPresenter.getListChat();
    }
  }

  @override
  void getChat(ResChat resChat) {
    setState(() {
      chatInfo=resChat;
    });
  }

  @override
  void getListChat(List<ResChat> data) {
    if(data!=null && data.length>0){
      setState(() {
        listResChat.clear();
        listResChat.addAll(data);
      });
      _chatPresenter.checkMess(idReceiver, idSender, listResChat);
    }
  }

  @override
  void showProgress(bool b) {
    // TODO: implement showProgress
  }
}
