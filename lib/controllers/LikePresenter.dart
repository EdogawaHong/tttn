import 'package:firebase_database/firebase_database.dart';
import 'package:tttn/commons/Constance.dart';
import 'package:tttn/commons/Utils.dart';
import 'package:tttn/controllers/LikeInf.dart';
import 'package:tttn/model/ResUser.dart';
import 'package:tttn/model/User.dart';

class LikePresenter {
  LikeInf inf;

  LikePresenter(this.inf);

  getListUser(List<int> like) async {
    inf.showProgress(true);
    bool check = await Utils.hasConnection();
    if (check) {
      List<ResUser> listUser = List();
      List<ResUser> listUserLike = List();
      DatabaseReference _reference =
          FirebaseDatabase.instance.reference().child(Constance.user);
      await _reference.once().then((DataSnapshot snapshot) {
        print(snapshot.value.entries.length);
        for (var val in snapshot.value.entries) {
          print(val.value.toString());
          User user = User(
              idUser: val.value[Constance.idUser],
              name: val.value[Constance.name],
              phone: val.value[Constance.phone],
              password: val.value[Constance.password],
              avatar: val.value[Constance.avatar],
              email: val.value[Constance.email]);
          print("val.key: ${val.key}");
          String key = val.key;
          listUser.add(ResUser(key: key, user: user));
          print("listUser.add: ${ResUser(key: key, user: user).toJson()}");
        }
        print('list: ${listUser.length}');
      });
      listUser.forEach((e) {
        like.forEach((e1) {
          if (e1 == e.user.idUser) listUserLike.add(e);
        });
      });
      inf.getListLike(listUserLike);
      inf.showProgress(false);
    } else {
      inf.getListLike(null);
      inf.showProgress(false);
    }
  }
}
