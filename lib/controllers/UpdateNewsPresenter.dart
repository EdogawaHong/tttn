import 'package:firebase_database/firebase_database.dart';
import 'package:tttn/commons/Constance.dart';
import 'package:tttn/controllers/UpdateNewsInf.dart';
import 'package:tttn/model/ResNews.dart';
import 'package:tttn/model/User.dart';

class UpdateNewsPresenter {
  UpdateNewsInf inf;

  UpdateNewsPresenter(this.inf);

  updateNews(ResNews resNews) {
    FirebaseDatabase.instance
        .reference()
        .child(Constance.news)
        .child(resNews.key)
        .update(resNews.news.toJson())
        .then((value) {
      inf.updateNewsSuccess(true, resNews);
    }, onError: (e) {
      inf.updateNewsSuccess(false, resNews);
    });
  }

  updateStatusLike(int id,ResNews resNews){
    if(resNews.news.like==null) {
      List<int> likes=List();
      likes.add(id);
      resNews.news.like=likes;
    }else{
      int index = resNews.news.like.indexOf(id);
      if (index < 0) {
        resNews.news.like.add(id);
      } else {
        resNews.news.like.removeAt(index);
      }
    }
    updateNews(resNews);
  }
}
