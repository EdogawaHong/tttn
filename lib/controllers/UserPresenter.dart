import 'package:firebase_database/firebase_database.dart';
import 'package:tttn/commons/AppConstant.dart';
import 'package:tttn/commons/Constance.dart';
import 'package:tttn/commons/SharedPref.dart';
import 'package:tttn/commons/Utils.dart';
import 'package:tttn/controllers/UserInf.dart';
import 'package:tttn/model/Offer.dart';
import 'package:tttn/model/ResUser.dart';
import 'package:tttn/model/User.dart';

class UserPresenter{
  UserInf inf;

  UserPresenter(this.inf);


  getListUsers(){
    List<ResUser> listUser=List();
    DatabaseReference _reference =
    FirebaseDatabase.instance.reference().child(Constance.user);
    _reference.once().then((DataSnapshot snapshot) {
      print(snapshot.value.entries.length);
      for (var val in snapshot.value.entries) {
        print(val.value.toString());
        Offer offer;
        if(val.value[Constance.offer]!=null){
          offer=Offer(
            idOffer: val.value[Constance.offer][Constance.idOffer],
            address: val.value[Constance.offer][Constance.address],
            area: val.value[Constance.offer][Constance.area],
            price: val.value[Constance.offer][Constance.price],
          );
        }
        User user=User(
          idUser: val.value[Constance.idUser],
          name: val.value[Constance.name],
          phone: val.value[Constance.phone],
          password: val.value[Constance.password],
          avatar: val.value[Constance.avatar],
          email: val.value[Constance.email],
          offer: offer,
        );
        print("val.key: ${val.key}");
        String key=val.key;
        listUser.add(ResUser(key: key,user: user));
        print("listUser.add: ${ResUser(key: key,user: user).toJson()}");
      }
      print('list: ${listUser.length}');
      inf.getListUser(listUser);
    });

  }

  checkAccount(String phone,String pass,List<ResUser> listUser) async {
    if (phone.isEmpty || pass.isEmpty) {
      Utils.showToast("Vui lòng nhập đủ thông tin!");
    } else {
      bool check=await Utils.hasConnection();
      if(check) {
        if (Utils.checkPhoneNumber(phone)) {
          inf.showProgress(true);
          print("list res user: ${listUser.length}");
          int d=0;
          for(int i=0;i<listUser.length;i++){
            print('account login: $phone $pass');
            if (phone == listUser[i].user.phone && pass==listUser[i].user.password){
              await SharedPref.save(Constance.user_storage, listUser[i]);
              inf.loginSuccess(listUser[i]);
              AppConstant.user=listUser[i];
              AppConstant.idUser=listUser[i].user.idUser;
              d++;
              break;
            }
          }
          if (d==0) {
            inf.loginSuccess(null);
            Utils.showToast("Vui lòng kiểm tra thông tin đăng nhập!");
          }
          inf.showProgress(false);
        } else {
          Utils.showToast("Số điện thoại không hợp lệ!");
        }
      }
    }
  }

  register(String phone, String pass, String repeat,List<ResUser> listUser) async{
    if (phone.isEmpty || pass.isEmpty || repeat.isEmpty) {
      Utils.showToast("Vui lòng nhập đủ thông tin!");
    } else {
      if (pass != repeat) {
        Utils.showToast("Mật khẩu không trùng khớp!");
      } else {
        inf.showProgress(true);
        bool check=await Utils.hasConnection();
        if(check) {
          if (Utils.checkPhoneNumber(phone)) {
            if (checkPhoneInDB(listUser, phone)) {
              inf.showProgress(false);
              Utils.showToast("Số điện thoại đã được sử dụng!");
            } else {
              User user = User(
                  idUser:listUser.length,name: "user$phone", phone: phone, password: pass);
              FirebaseDatabase.instance
                  .reference()
                  .child(Constance.user)
                  .push()
                  .set(user.toJson())
                  .then((value) async {
                inf.showProgress(false);
                inf.registerSuccess(true);
              }).catchError((onError) {
                inf.showProgress(false);
                inf.registerSuccess(false);
                Utils.showToast("Có lỗi xảy ra!");
              });
            }
          } else {
            Utils.showToast("Vui lòng kiểm tra lại số điện thoại!");
          }
        }
      }
    }
  }

  checkPhoneInDB(List<ResUser> users, String phone) {
    for(int i=0;i<users.length;i++){
      if (phone == users[i].user.phone) return true;
    }
    return false;
  }
}