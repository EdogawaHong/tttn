import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:tttn/commons/Constance.dart';
import 'package:tttn/commons/Utils.dart';
import 'package:tttn/controllers/ChatInf.dart';
import 'package:tttn/model/Chat.dart';
import 'package:tttn/model/ChatItem.dart';
import 'package:tttn/model/ResChat.dart';

class ChatPresenter {
  ChatInf inf;

  ChatPresenter(this.inf);

  getListChat() async {
    inf.showProgress(true);
    bool check = await Utils.hasConnection();
    if (check) {
      List<ResChat> list = List();
      DatabaseReference _reference =
      FirebaseDatabase.instance.reference().child(Constance.chat);
      _reference.once().then((DataSnapshot snapshot) {
        print(snapshot.value.entries.length);
        if(snapshot.value.entries!=null) {
          for (var val in snapshot.value.entries) {
            print(val.value.toString());
            List<ChatItem> items = List();
            List<dynamic> listItem = val.value[Constance.items];
            for (int i = 0; i < listItem.length; i++) {
              ChatItem item = ChatItem(
                  idMessDetail: listItem[i][Constance.idMessDetail],
                  content: listItem[i][Constance.content],
                  time: listItem[i][Constance.time],
                  idUser: listItem[i][Constance.idUser]
              );
              items.add(item);
            }
            Chat chat = Chat(
              idMess: val.value[Constance.idMess],
              idReceiver: val.value[Constance.idReceiver],
              idSender: val.value[Constance.idSender],
              items: items,
            );
            print("val.key: ${val.key}");
            String key = val.key;
            list.add(ResChat(key: key, chat: chat));
            print("list chat add: ${ResChat(key: key, chat: chat).toJson()}");
          }
          print('list chat: ${list.length}');
          inf.getListChat(list);
        }
      });
      inf.showProgress(false);
    } else {
      inf.showProgress(false);
    }
  }

  addChat(int idReceiver, int idSender, ChatItem item) async {
    //inf.showProgress(true);
    bool check = await Utils.hasConnection();
    if (check) {
      Chat chat = Chat(
          idSender: idSender,
          idReceiver: idReceiver,
          items: [item]
      );
      FirebaseDatabase.instance
          .reference()
          .child(Constance.chat)
          .push()
          .set(chat.toJson())
          .then((value) async {
        //inf.showProgress(false);
        inf.addChat(true);
      }).catchError((onError) {
        //inf.showProgress(false);
        inf.addChat(false);
        Utils.showToast("Có lỗi xảy ra!");
      });
    }
  }

  checkMess(int idReceiver, int idSender,List<ResChat> list){
    for(int i=0;i<list.length;i++){
      if((list[i].chat.idReceiver==idReceiver ||list[i].chat.idReceiver==idSender) &&
          (list[i].chat.idSender==idReceiver ||list[i].chat.idSender==idSender)) {
        inf.getChat(list[i]);
        return;
      }
    }
    inf.getChat(null);
  }

  sendMess(ResChat resChat){
      FirebaseDatabase.instance
          .reference()
          .child(Constance.chat)
          .child(resChat.key)
          .update(resChat.chat.toJson())
          .then((value) {
        inf.addChat(true);
      }, onError: (e) {
        inf.addChat(false);
      });
  }
}