import 'package:firebase_database/firebase_database.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:tttn/commons/AppConstant.dart';
import 'package:tttn/commons/Constance.dart';
import 'package:tttn/commons/Utils.dart';
import 'package:tttn/model/Report.dart';

class ReportPresenter {

  static createNews(String content, String idNews) {
    final referenceDatabase = FirebaseDatabase.instance;
    if (content != null && content.isNotEmpty) {
      Report report = Report(
        content: content,
        time: DateFormat("HH:mm dd/MM/yyyy").format(DateTime.now()),
        idNews: idNews,
        idUser: AppConstant.idUser.toString(),
      );
      referenceDatabase
          .reference()
          .child(Constance.report)
          .push()
          .set(report.toJson())
          .then((value) async {
        Get.back();
        Utils.showToast("Gửi báo cáo thành công!");
      }).catchError((onError) {
        Get.back();
        Utils.showToast("Có lỗi xảy ra! Vui lòng thử lại sau!");
      });
    }
  }
}
