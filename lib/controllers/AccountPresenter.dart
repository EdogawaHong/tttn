import 'package:firebase_database/firebase_database.dart';
import 'package:get/get.dart';
import 'package:tttn/commons/Constance.dart';
import 'package:tttn/commons/SharedPref.dart';
import 'package:tttn/commons/Utils.dart';
import 'package:tttn/controllers/AccountInf.dart';
import 'package:tttn/model/ResUser.dart';
import 'package:tttn/model/User.dart';

class AccountPresenter {
  AccountInf inf;

  AccountPresenter(this.inf);

  loadUserInfo() async {
    try {
      ResUser info =
          ResUser.fromJson(await SharedPref.read(Constance.user_storage));
      inf.getInfoAccount(info);
      print('user sharedpref: ${info.toJson()}');
    } catch (err) {
      inf.getInfoAccount(null);
    }
  }

  updateAccount(ResUser resUser) {
    FirebaseDatabase.instance.reference().child(Constance.user).child(resUser.key)
        .update(resUser.user.toJson()).then((value) {
      inf.updateAccountStatus(true,resUser);
    }, onError: (e) {
      inf.updateAccountStatus(false,resUser);
    });
  }
}
