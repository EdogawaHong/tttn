import 'package:firebase_database/firebase_database.dart';
import 'package:tttn/commons/Constance.dart';
import 'package:tttn/commons/Utils.dart';
import 'package:tttn/controllers/NewsDetailInf.dart';
import 'package:tttn/model/Cmt.dart';
import 'package:tttn/model/News.dart';
import 'package:tttn/model/ResNews.dart';
import 'package:tttn/model/UserInfo.dart';

class NewsDetailPresenter {
  NewsDetailInf inf;

  NewsDetailPresenter(this.inf);

  getNewsDetail(String key) async {
    inf.showProgress(true);
    bool check = await Utils.hasConnection();
    if (check) {
      FirebaseDatabase.instance
          .reference()
          .child(Constance.news)
          .child(key)
          .once()
          .then((DataSnapshot snapshot) {
        print("news detail: ${snapshot.toString()}");
        if (snapshot.value[Constance.user] != null) {
          UserInfo userInfo = UserInfo(
            idUser: snapshot.value[Constance.user][Constance.idUser],
            name: snapshot.value[Constance.user][Constance.name],
            avatar: snapshot.value[Constance.user][Constance.avatar],
            phone: snapshot.value[Constance.user][Constance.phone],
          );
          List<dynamic> images = snapshot.value[Constance.images];
          List<dynamic> likes = snapshot.value[Constance.like];
          List<dynamic> resCmts = snapshot.value[Constance.cmt];
          List<Cmt> cmts = List();
          if (resCmts != null && resCmts.length > 0) {
            for (int i = 0; i < resCmts.length; i++) {
              Cmt cmt = Cmt(
                  idCmt: resCmts[i][Constance.idCcmt],
                  content: resCmts[i][Constance.content],
                  time: resCmts[i][Constance.time],
                  user: UserInfo(
                    idUser: resCmts[i][Constance.user][Constance.idUser],
                    name: resCmts[i][Constance.user][Constance.name],
                    avatar: resCmts[i][Constance.user][Constance.avatar],
                    phone: resCmts[i][Constance.user][Constance.phone],
                  ));
              cmts.add(cmt);
            }
          }
          News news = News(
              idNews: snapshot.value[Constance.idNews],
              user: userInfo,
              price: snapshot.value[Constance.price],
              area: snapshot.value[Constance.area],
              address: snapshot.value[Constance.address],
              lat: snapshot.value[Constance.lat],
              lng: snapshot.value[Constance.lng],
              desc: snapshot.value[Constance.desc],
              time: snapshot.value[Constance.time],
              status: snapshot.value[Constance.status],
              images: (images != null) ? images.cast<String>().toList() : null,
              like: (likes != null) ? likes.cast<int>().toList() : null,
              cmt: cmts);
          print("res news: ${ResNews(key: key, news: news).toJson()}");
          inf.getNewsDetailSuccess(ResNews(key: key, news: news));
        }
        inf.getNewsDetailSuccess(null);
      }, onError: (e) {
        inf.getNewsDetailSuccess(null);
      });
      inf.showProgress(false);
    } else {
      inf.showProgress(false);
      inf.getNewsDetailSuccess(null);
    }
  }
}
