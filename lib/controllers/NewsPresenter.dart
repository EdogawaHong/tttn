import 'package:firebase_database/firebase_database.dart';
import 'package:tttn/commons/AppConstant.dart';
import 'package:tttn/commons/Constance.dart';
import 'package:tttn/commons/Utils.dart';
import 'package:tttn/controllers/NewsInf.dart';
import 'package:tttn/controllers/ProfileInf.dart';
import 'package:tttn/model/Cmt.dart';
import 'package:tttn/model/News.dart';
import 'package:tttn/model/ResNews.dart';
import 'package:tttn/model/UserInfo.dart';

class NewsPresenter {
  NewsInf inf;

  NewsPresenter(this.inf);

  getNews() async {
    inf.showProgress(true);
    bool check = await Utils.hasConnection();
    if (check) {
      List<ResNews> listNews = List();
      DatabaseReference _reference =
          FirebaseDatabase.instance.reference().child(Constance.news);
      await _reference.once().then((DataSnapshot snapshot) {
        print(snapshot.value.entries.length);
        for (var val in snapshot.value.entries) {
          print(val.value.toString());
          print(snapshot.key);
          print("val.key: ${val.key}");
          UserInfo userInfo;
          if (val.value[Constance.user] != null) {
            userInfo = UserInfo(
              idUser: val.value[Constance.user][Constance.idUser],
              name: val.value[Constance.user][Constance.name],
              avatar: val.value[Constance.user][Constance.avatar],
              phone: val.value[Constance.user][Constance.phone],
            );
          }
          List<dynamic> images = val.value[Constance.images];
          List<dynamic> likes = val.value[Constance.like];
          List<dynamic> resCmts = val.value[Constance.cmt];
          List<Cmt> cmts = List();
          if (resCmts != null && resCmts.length > 0) {
            for (int i = 0; i < resCmts.length; i++) {
              Cmt cmt = Cmt(
                  idCmt: resCmts[i][Constance.idCcmt],
                  content: resCmts[i][Constance.content],
                  time: resCmts[i][Constance.time],
                  user: UserInfo(
                    idUser: resCmts[i][Constance.user][Constance.idUser],
                    name: resCmts[i][Constance.user][Constance.name],
                    avatar: resCmts[i][Constance.user][Constance.avatar],
                    phone: resCmts[i][Constance.user][Constance.phone],
                  ));
              cmts.add(cmt);
              print("cmt list news: ${cmt.toJson()}");
            }
          }
          News news = News(
              idNews: val.value[Constance.idNews],
              user: userInfo,
              price: val.value[Constance.price],
              area: val.value[Constance.area],
              address: val.value[Constance.address],
              lat: val.value[Constance.lat],
              lng: val.value[Constance.lng],
              desc: val.value[Constance.desc],
              time: val.value[Constance.time],
              status: val.value[Constance.status],
              images: (images != null) ? images.cast<String>().toList() : null,
              like: (likes != null) ? likes.cast<int>().toList() : null,
              cmt: cmts);
          listNews.add(ResNews(key: val.key, news: news));
        }
        print('list: ${listNews.length}');
      });
      inf.getNews(listNews);
      inf.showProgress(false);
    } else {
      inf.getNews(null);
      inf.showProgress(false);
    }
  }
}
