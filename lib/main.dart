import 'package:fbroadcast/fbroadcast.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tttn/commons/Utils.dart';
import 'package:tttn/model/ResUser.dart';
import 'package:tttn/model/User.dart';
import 'package:tttn/view/login/LoginView.dart';

import 'commons/AppConstant.dart';
import 'commons/Constance.dart';
import 'commons/DialogCustom.dart';
import 'commons/SharedPref.dart';
import 'view/home/HomeView.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
          primarySwatch: Colors.deepOrange, fontFamily: 'JosefinSans'),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> disappear;

  ResUser user;

  Future navigateToSubPage(context) async {
    Widget page = HomeView();
    if (user == null) {
      page = LoginView();
    }
    Navigator.pushReplacement(
      context,
      PageRouteBuilder(
        pageBuilder: (c, a1, a2) => page,
        transitionsBuilder: (c, anim, a2, child) =>
            FadeTransition(opacity: anim, child: child),
        transitionDuration: Duration(milliseconds: 1000),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    initView();
  }

  initView() async {
    bool checkNetwork = await Utils.hasConnection();
    if (checkNetwork) {
      initFirebase();
      Utils.getUser().then((value) {
        setState(() {
          user = value;
          AppConstant.user=value;
          if(user!=null) {
            print('main user: ${user.toJson()}');
            AppConstant.idUser=user.user.idUser;
          }
          else print('main user: null');
        });
      });
      _controller = AnimationController(
          duration: Duration(milliseconds: 800), vsync: this);
      disappear = CurvedAnimation(
        parent: _controller,
        curve: Curves.easeIn,
      );
      _controller.addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          Future.delayed(Duration(milliseconds: 100), () {
            navigateToSubPage(context);
          });
        }
      });
      _controller.forward();
    } else {
      showNotification(context);
    }
  }

  initFirebase() {
    Firebase.initializeApp();
    //initFirebaseMessage();
  }

  // final FirebaseMessaging fm = FirebaseMessaging();
  //
  // initFirebaseMessage() async {
  //   fm.configure(onMessage: (Map<String, dynamic> message) async {
  //     print("onMesage: $message");
  //     FBroadcast.instance().broadcast(Constance.key_noti, value: message);
  //   }, onLaunch: (Map<String, dynamic> message) async {
  //     print("onLaunch: $message");
  //     FBroadcast.instance().broadcast(Constance.key_noti, value: message);
  //   }, onResume: (Map<String, dynamic> message) async {
  //     print("onResume: $message");
  //     FBroadcast.instance().broadcast(Constance.key_noti, value: message);
  //   });
  //   fm.requestNotificationPermissions(const IosNotificationSettings(
  //       sound: true, badge: true, alert: true, provisional: true));
  //   fm.onIosSettingsRegistered.listen((IosNotificationSettings settings) {
  //     print("Settings registered: $settings");
  //   });
  //   fm.getToken().then((String token) {
  //     // assert(token != null);
  //     // Constance.fcm_key = token;
  //     // print("Push Messaging token: $token");
  //   });
  // }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          child: Icon(Icons.home,color: Colors.white,size: 300,),
            width: Get.width,
            height: Get.height,
            decoration: BoxDecoration(
                gradient: LinearGradient(begin: Alignment.topCenter, colors: [
              Colors.yellow[900],
              Colors.yellow[700],
              Colors.yellow[400]
            ]))));
  }

  showNotification(context) {
    Get.dialog(
      DialogCustom(
        title: "Thông báo",
        description: 'Kiểm tra kết nối internet và thử lại!',
        buttonAcceptText: "OK",
        buttonDeclineText: null,
        color: Colors.white,
        onPressButtonAccept: (() {
          Navigator.of(context).pop();
          initView();
        }),
        onPressButtonDecline: (() {}),
      ),
    );
  }
}
