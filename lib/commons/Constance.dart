class Constance{
  static String key_noti="notification";
  static String fcm_key="fcm_key";

  static String user_storage="user";
  static String id_user="id_user";

  static String user="user";
  static String idUser="id_user";
  static String phone="phone";
  static String name="name";
  static String email="email";
  static String password="password";
  static String avatar="avatar";
  static String offer="offer";
  static String idOffer="id_offer";
  static String address="address";
  static String area="area";
  static String price="price";

  static String news="news";
  static String idNews="id_news";
  static String lat="lat";
  static String lng="lng";
  static String desc="desc";
  static String time="time";
  static String status="status";
  static String images="images";
  static String like="like";
  static String cmt="cmt";
  static String content="content";
  static String idCcmt="id_cmt";

  static String chat="chat";
  static String idMess="id_mess";
  static String idReceiver="id_receiver";
  static String idSender="id_sender";
  static String idMessDetail="id_mess_detail";
  static String items="items";

  static String report="report";
}