import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'AppConstant.dart';
import 'Utils.dart';

class GoogleMapsServices {
  var apiKey = AppConstant.keyGoogleMap;

  Future<String> getRouteCoordinates({
    LatLng origin,
    LatLng destination,
    BuildContext context,
  }) async {
    try {
      String url =
          "https://maps.googleapis.com/maps/api/directions/json?origin=${origin.latitude},${origin.longitude}&destination=${destination.latitude},${destination.longitude}&key=$apiKey";
      http.Response response = await http.get(Uri.parse(url));
      Map values = jsonDecode(response.body);
      if (values["status"] == "OK") {
        return values["routes"][0]["overview_polyline"]["points"];
      } else {
        Utils.showToast(
            'Không định tuyến được quãng đường. Vui lòng thử lại sau!');
      }
    } catch (error) {
      print('GET DIRECTION ERROR: $error');
      Utils.showToast('Có lỗi xảy ra trong quá trình định tuyến.');
    }
  }
}