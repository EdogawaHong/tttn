import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:tttn/model/ResUser.dart';

import 'AppConstant.dart';
import 'Constance.dart';
import 'SharedPref.dart';
class Utils{
  static moveToScreen(Widget widget, BuildContext context) {
    Navigator.push(
      context,
      CupertinoPageRoute(builder: (context) => widget),
    );
  }

  static moveToScreenCallback(
      Widget widget, BuildContext context, Function function) {
    Navigator.push(
      context,
      CupertinoPageRoute(builder: (context) => widget),
    ).then((value) {
      function.call();
    });
  }

  static moveAndReplaceToScreen(Widget widget, BuildContext context) {
    Navigator.pushReplacement(
      context,
      CupertinoPageRoute(builder: (context) => widget),
    );
  }

  static showToast(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0);
  }
  static checkPhoneNumber(String phone) {
    if (phone.length != 10) {
      return false;
    } else {
      if (phone[0] != '0') {
        return false;
      } else {
        try {
          int p = int.parse(phone);
          return true;
        } catch (e) {
          return false;
        }
      }
    }
  }

  static Future<bool> hasConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        return true;
      } else {
        Utils.showToast('Vui lòng kiểm tra kết nối internet!');
        return false;
      }
    } on SocketException catch (_) {
      print('not connected');
      Utils.showToast('Vui lòng kiểm tra kết nối internet!');
      return false;
    }
  }

  static Future<ResUser> getUser() async {
    try {
      ResUser info =
      ResUser.fromJson(await SharedPref.read(Constance.user_storage));
      return info;
    }catch(e){
      return null;
    }
  }

  static Widget CircularProgress(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(
        strokeWidth: 3.0,
        valueColor: AlwaysStoppedAnimation(AppConstant.colorPrimary),
      ),
    );
  }
  static Future<LatLng> getLocation() async {
    final location = Location();
    bool _locationServiceEnabled = await location.serviceEnabled();
    if (!_locationServiceEnabled) {
      _locationServiceEnabled = await location.requestService();
      if (!_locationServiceEnabled) {
        return null;
      }
    }
    PermissionStatus _locationPermissionGranted =
    await location.hasPermission();
    if (_locationPermissionGranted == PermissionStatus.denied) {
      _locationPermissionGranted = await location.requestPermission();
      if (_locationPermissionGranted != PermissionStatus.granted) {
        return null;
      }
    }

    final curLocation = await location.getLocation();
    LatLng _currentLatLng = LatLng(curLocation.latitude, curLocation.longitude);
    print('LOCATION: $_currentLatLng');
    return _currentLatLng;
  }

  static String formatMoney(num money) {
    if (money != null && money > 0) {
      final formatter = new NumberFormat("#,###");
      var finalMoney = formatter.format(money);
      if (finalMoney == null || finalMoney.isEmpty) {
        finalMoney = '0';
      }
      return finalMoney;
    } else {
      return "0";
    }
  }

  static Widget borderBottom() {
    return Container(
      height: 1,
      color: Colors.grey,
      margin: EdgeInsets.only(top: 8, bottom: 16),
    );
  }

  static checkGps() async {
    var enabled = await Geolocator().isLocationServiceEnabled();
    return enabled;
  }

  static getEmpty(BuildContext context) {
    return Center(
      child: Container(
        margin: EdgeInsets.only(top: 50),
        color: Colors.transparent,
        child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.inbox,
                color: AppConstant.colorPrimary,
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                "Hiện không có dữ liệu!",
                style: TextStyle(
                    fontSize: 20,
                    color: AppConstant.colorPrimary,
                    fontWeight: FontWeight.bold),
              )
            ]),
      ),
    );
  }
}