import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tttn/model/ResUser.dart';
import 'package:tttn/model/User.dart';

import 'Constance.dart';
import 'SharedPref.dart';

class AppConstant {
  static String BASE_URL = "https://demo4227448.mockable.io/";
  static String keyGoogleMap='AIzaSyA1rTRYFDlpmH3rCK36ZvaRpyY1jw7ZwoA';
  static double sizeText = 20;
  static Color colorPrimary = Colors.yellow[900];

  static TextStyle hintStyle = TextStyle(
    fontSize: 20,
    color: Colors.black54,
  );
  static TextStyle inputStyle = TextStyle(
    fontSize: 20,
    color: Colors.black,
  );
  static TextStyle labelStyle = TextStyle(
    fontSize: 15,
    color: Colors.deepOrange,
  );
  static TextStyle actionStyle = TextStyle(
    fontSize: 25,
    color: Colors.white,
  );
  static TextStyle textStyle = TextStyle(
    fontSize: 20,
    color: Colors.black,
  );
  static TextStyle infoStyle = TextStyle(
    fontSize: 20,
    color: Colors.black54,
  );
static TextStyle timeStyle=TextStyle(fontSize: 14,color: Colors.grey);

static ResUser user;
 static int idUser=-1;
}
