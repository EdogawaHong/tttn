import 'package:flutter/material.dart';

import 'AppConstant.dart';

class DialogCustom extends StatelessWidget {
  final String title, description, buttonDeclineText, buttonAcceptText;
  final Color color;
  VoidCallback onPressButtonDecline, onPressButtonAccept;

  DialogCustom(
      {@required this.title,
        @required this.description,
        @required this.buttonDeclineText,
        @required this.buttonAcceptText,
        @required this.onPressButtonDecline,
        @required this.onPressButtonAccept,
        this.color});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          decoration: new BoxDecoration(
              color: AppConstant.colorPrimary,
              borderRadius: new BorderRadius.only(
                topLeft: const Radius.circular(5.0),
                topRight: const Radius.circular(5.0),
              )),
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 10),
          child: Row(
            children: [
              Icon(
                Icons.info,
                color: Colors.white,
              ),
              SizedBox(width: 8,),
              Text(
                "$title",
                textAlign: TextAlign.start,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20.0,
                    fontWeight: FontWeight.w700),
              ),
            ],
          ),
        ),
        Container(
          decoration: new BoxDecoration(
              color: Colors.white,
              borderRadius: new BorderRadius.only(
                bottomLeft: const Radius.circular(5.0),
                bottomRight: const Radius.circular(5.0),
              )),
          padding: EdgeInsets.only(left: 8, right: 8, bottom: 8),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  description,
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: AppConstant.sizeText,
                  ),
                ),
              ),
              buttonAction(),
            ],
          ),
        ),
      ],
    );
  }

  IntrinsicHeight buttonAction() {
    return IntrinsicHeight(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          (buttonDeclineText != null)
              ?  Flexible(
              child: InkWell(
                onTap: () {
                  onPressButtonDecline();
                },
                child: Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.symmetric(horizontal: 4),
                  padding:
                  EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  decoration: BoxDecoration(
                      color: AppConstant.colorPrimary,
                      border: Border.all(
                          width: 2, color: AppConstant.colorPrimary),
                      borderRadius: BorderRadius.circular(20)),
                  child: Column(
                    children: [
                      Text(
                        '${buttonDeclineText}'.toUpperCase(),
                        style: TextStyle(
                            fontSize: 16,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              )
          ): Container(),
          (buttonAcceptText != null)
              ? Flexible(
              child: InkWell(
                onTap: () {
                  onPressButtonAccept();
                },
                child: Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.symmetric(horizontal: 4),
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  decoration: BoxDecoration(
                      color: AppConstant.colorPrimary,
                      border: Border.all(
                          width: 2, color: AppConstant.colorPrimary),
                      borderRadius: BorderRadius.circular(20)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        '${buttonAcceptText}'.toUpperCase(),
                        overflow: TextOverflow.visible,
                        maxLines: 2,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16,
                            color: (color != null) ? color : Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ))
              : Container(),
        ],
      ),
    );
  }
}